using LinearAlgebra
using SparseArrays
using bamg
using Test
using PETScBinaryIO
using BALUtils
using IterativeSolvers
using Libdl
using ForwardDiff
using HDF5

dlopen("../../ba-tao/build/libderivatives.dylib")

function ceres_derivative(observed, camera, point)
    params = vcat(observed, camera, point)
    residuals = zeros(2)
    jacobian = zeros(24)
    ccall(:compute_derivatives, Int, (Ptr{Float64}, Ptr{Float64}, Ptr{Float64}), params, residuals, jacobian)
    camera_jac = transpose(reshape(jacobian[1:9*2], 9, 2))
    point_jac = transpose(reshape(jacobian[9*2+1:end], 3, 2))
    residuals, camera_jac, point_jac
end

function readmat(filename, name)
    sz = h5read(filename, "$name/size")
    colptr = Int.(h5read(filename, "$name/jc")) .+ 1
    nzval = h5read(filename, "$name/data")
    rowval = Int.(h5read(filename, "$name/ir")) .+ 1
    SparseMatrixCSC(sz..., colptr, rowval, nzval)
end

@testset "All" begin
    @testset "Block matrix" begin
        A = rand(3*2, 3*2)
        B = bamg.block_diag(A, 3)
        @test Matrix(B * inv(B)) ≈ Matrix{eltype(A)}(I, size(A)...)
        @test Matrix(B) == Matrix(SparseMatrixCSC(B))
        @test B[3,3] == A[3,3]
        @test B[10, 0] == 0

        x = rand(size(B,1))
        @test B * x ≈ Matrix(B) * x

        C = [1 2 3 0 0 0
             4 5 6 0 0 0
             7 8 9 0 0 0
             0 0 0 1 2 3
             0 0 0 4 5 6
             0 0 0 7 8 9]
        @test Matrix(bamg.block_diag(C, 3)) == C

        for i in 1:6
            for j in 1:6
                @test C[i,j] == Matrix(C)[i,j]
            end
        end
    end

    @testset "Nest matrix" begin
        x = reshape([rand(4, 3), rand(2, 3), rand(6, 3),
                     rand(4, 5), rand(2, 5), rand(6, 5)], 3, 2)
        A = bamg.NestMatrix(x)
        AA = [x[1,1] x[1,2]
              x[2,1] x[2,2]
              x[3,1] x[3,2]]
        @test A ≈ AA

        @test bamg.squared_column_norm(A) ≈ mapreduce(x->x^2,+,AA,dims=1)[:]
        @test bamg.squared_column_norm(A) ≈ bamg.squared_column_norm(AA)
        @test length(bamg.squared_column_norm(A)) == size(A,2)
        A_ = copy(A)
        AA_ = copy(AA)
        bamg.diagscale!(A_, nothing, bamg.squared_column_norm(A))
        bamg.diagscale!(AA_, nothing, bamg.squared_column_norm(A))
        @test A_ ≈ AA_
        @test A_ ≈ AA * Diagonal(bamg.squared_column_norm(A))
        @test !(A_ ≈ A)

        rhs = rand(8)
        @test A * rhs ≈ AA * rhs
        rhs = rand(12)
        @test A' * rhs ≈ AA' * rhs

        B = bamg.DelayedMatrixProduct(A)
        BB = AA' * AA
        rhs = rand(size(B, 2))
        @test B * rhs ≈ BB * rhs
        @test diag(B) ≈ diag(BB)
    end

    @testset "SparseMatrixBCOO" begin
        A = bamg.SparseMatrixBCOO(4,6,(2,3),2)
        A.coords[1] = (1,1)
        A.blocks[1] = [1 2 3; 4 5 6]
        A.coords[2] = (3,4)
        A.blocks[2] = [7 8 9; 10 11 12]
        B = [1.0 2 3 0 0 0;
             4   5 6 0 0 0;
             0   0 0 7 8 9;
             0   0 0 10 11 12]
        @test A ≈ B
        @test norm(SparseMatrixCSC(A) - B, 1) ≈ 0.0
        x = rand(size(A,1))
        @test A' * x ≈ B' * x
        x = rand(size(A,1))
        @test bamg.NestMatrix(reshape([A, A],1,2))' * x ≈ hcat(B, B)' * x

        A_ = copy(A)
        B_ = copy(B)
        d = collect(1:size(A,2))
        @test rmul!(A_, Diagonal(d)) ≈ rmul!(B_, Diagonal(d))

        @test bamg.block_diag(A) ≈ bamg.block_diag(B' * B, 3)

        AA = bamg.DelayedMatrixProduct(A)
        BB = bamg.DelayedMatrixProduct(B)
        @test Matrix(AA) ≈ Matrix(BB)
        @test Matrix(AA) ≈ B'*B

        x = rand(size(AA,2))
        AA.D = d
        BB.D = d
        @test AA * x ≈ Matrix(AA) * x
        @test AA * x ≈ (B'*B+Diagonal(d)) * x

        C = bamg.DelayedMatrixProduct(bamg.NestMatrix(reshape([A, A],1,2)))
        C.D = rand(size(C,2))
        D = hcat(B,B)' * hcat(B, B) + Diagonal(C.D)
        @test Matrix(C) ≈ D
        x = rand(size(D,2))
        @test C * x ≈ D * x

        @test Matrix(bamg.diagscale!(AA, d, d)) ≈ Matrix(bamg.diagscale!(BB, d, d))
    end

    @testset "Schur Complement" begin
        E = bamg.SparseMatrixBCOO(6,6,(2,3),3)
        E.coords[1] = (1,1)
        E.blocks[1] = [-0.0230889 0.0032882 0.528441
                       0.200621   0.560846  0.0181275]
        E.coords[2] = (3,1)
        E.blocks[2] = [0.771047   0.00201108  0.99842
                       0.6367     0.999948    0.0552892]
        E.coords[3] = (5,4)
        E.blocks[3] = [0.771047   0.00201108  0.99842
                       0.6367     0.999948    0.0552892]
        F = bamg.SparseMatrixBCOO(6,8,(2,4),3)
        F.coords[1] = (1,1)
        F.blocks[1] = [-0.0398154   0.476741    0.295813  0.358158
                       -0.338896    0.0673042   0.304254  0.0]
        F.coords[2] = (3,1)
        F.blocks[2] = [-0.160642   -0.535296   -0.14164   0.616301
                       -0.635831    0.244771    0.636003  0.0]
        F.coords[3] = (5,5)
        F.blocks[3] = [-0.160642   -0.535296   -0.14164   0.616301
                       -0.635831    0.244771    0.636003  0.0]
        FE = bamg.NestMatrix(reshape([F, E], 1, 2))
        X = bamg.DelayedMatrixProduct(FE)
        X.D .= 100
        s = bamg.SchurComplement(1:8, 9:14, 4)
        r = rand(size(FE, 1))

        # b = -gradient = - J * r
        b = -vcat(F' * r, E' * r)
        S_, rhs_ = bamg.complement(s, X, b, r)
        S, rhs = bamg.complement(s, SparseMatrixCSC(X), b, r)
        @test S ≈ S_

        x = S \ rhs

        E_ = Matrix(E)
        F_ = Matrix(F)
        S_ = F_'*F_ + Diagonal(X.D[1:8]) - F_' * E_ * inv(E_' * E_ + Diagonal(X.D[9:14])) * E_' * F_
        @test S_ ≈ S
        rhs_ = b[1:8] - F_'*E_ * inv(E_' * E_ + Diagonal(X.D[9:14])) * b[9:14]
        @test rhs ≈ rhs_

        X_ = SparseMatrixCSC(X)
        S, rhs = bamg.complement(s, X, b, r)
        @test S ≈ S_
        @test rhs ≈ rhs_

        x_ = S_ \ rhs
        @test x_ ≈ x
        @test x_ ≈ S \ rhs

        y = bamg.back_substitute(s, X, b, x, r)
        y_ = bamg.back_substitute(s, X_, b, x, r)
        @test y ≈ y_
    end

    @testset "SparseMatrixBSC" begin
        A = [1 2 0 0
             3 4 0 0
             5 6 7 8
             9 1 2 3]
        B = bamg.SparseMatrixBSC([1, 1, 2], [1, 2, 2], 2, 2, 4, 4, Int)
        B.mat[1,1] = [1 2
                      3 4]
        B.mat[2,1] = [5 6
                      9 1]
        B.mat[2,2] = [7 8
                      2 3]

        @test SparseMatrixCSC(B) ≈ A
        d = [2, 3, 4, 5]
        @test Matrix(SparseMatrixCSC(B + Diagonal(d))) ≈ A + Diagonal(d)
        bd = bamg.block_diag(A, 2)
        @test Matrix(SparseMatrixCSC(B + bd)) ≈ A + bd
    end

    @testset "Derivative" begin
        ba = readbal("problem-49/problem.bal")
        tmp = zeros(Float64, 12)
        cfg = ForwardDiff.JacobianConfig(bamg.project, tmp, ForwardDiff.Chunk{12}())
        for (c, obs) in enumerate(ba.observations)
            for (p, x, y) in obs
                camera = vec(ba.cameras[c])
                point = ba.points[p]

                ceres_res, ceres_camera_jac, ceres_point_jac = ceres_derivative([x,y], camera, point)

                pp = bamg.project(vcat(camera, point))
                residuals = [x - pp[1], y - pp[2]]
                jac = ForwardDiff.jacobian(bamg.project, vcat(camera, point), cfg)
                camera_jac = jac[:, 1:9]
                point_jac = jac[:, 10:12]

                # ceres uses predicted - observed, we use observed - predicted.
                # This is why we have a negative sign on the residuals. The
                # jacobian gets a negative in the code, but we don't need it
                # here.
                if !(ceres_res ≈ -residuals)
                    @show camera, point, [x,y]
                    @show ceres_res + residuals
                end
                @test ceres_res ≈ -residuals
                @test ceres_camera_jac ≈ camera_jac
                @test ceres_point_jac ≈ point_jac
            end
        end
    end

    # @testset "Solver" begin
    #     ba = readbal("problem-49/problem.bal")
    #     bamg.create_multigrid(ba, Options())
    #     bamg.create_multigrid(convert(SparseMatrixCSC{Float64}, visibility_graph(ba)), Options())
    # end

    @testset "Nullspace" begin
        A = readpetsc("problem-49/S.petsc")[1]
        dscale = reshape(readpetsc("problem-49/dscale.petsc")[1][1:49*9], 9, :)
        poses = reshape(readpetsc("problem-49/solution.petsc")[1][1:49*9], 9, :)
        ns = bamg.nullspace(poses, dscale, false, 7)
        # ns can be ill conditioned, so we use qr on it first
        Q, _ = qr(reshape(permutedims(ns, [1,3,2]), :, size(ns, 2)))
        @test maximum(abs.(A * Matrix(Q))) <= 1e-13

        ba = readbal("5-block.bbal")
        _, Jf, sol = bamg.generate_problem(ba)
        J = Jf(sol).blocks[1]
        s = reshape(sol[1:num_cameras(ba)*9], 9, :)
        ns = bamg.nullspace(s, ones(size(s)), false, 7)
        # ns can be ill conditioned, so we use qr on it first
        Q, _ = qr(reshape(permutedims(ns, [1,3,2]), :, size(ns, 2)))
        @test maximum(abs.(J * Matrix(Q))) <= 1e-13
    end

    function read_mat(file, path)
        jc = Int64.(read(file, "$path/jc"))
        ir = Int64.(read(file, "$path/ir"))
        data = read(file, "$path/data")
        sz = read(file, "$path/size")
        SparseMatrixCSC(sz...,jc.+1,ir.+1,data)
    end

    @testset "Prolongation" begin
        file = h5open("../dump.mat")
        vis = read_mat(file, "0/visibility")
        mg = create_multigrid(vis, Options())
        m = map(x -> parse(Int, split(x, "/")[1]), names(file)) |> maximum
        for j in 1:m
            A = read_mat(file, "$j/A")
            dscale = read(file, "$j/scale")
            poses = read(file, "$j/poses")
            bamg.update!(mg, A, dscale, poses, nothing)

            for level in mg.levels
                if typeof(level) == bamg.AggregationLevel
                    for i in 1:7
                        ns = level.ns[:,i,:][:]
                        @test level.P * (level.R * ns) ≈ ns
                    end
                end
            end
        end
    end

    # @testset "Bundle Adjust" begin
    #     ba = readbal("problem-49/problem.bal")
    #     bamg.bundleadjust(ba)
    #     println("")
    #     bamg.bundleadjust(ba, solve=bamg.schur_solver(ba,bamg.lu_solver))
    #     println("")
    #     bamg.bundleadjust(ba, solve=bamg.schur_solver(ba,bamg.cg_solver()))
    #     println("")
    #     bamg.bundleadjust(ba, solve=bamg.schur_cg_solver(ba, bamg_solver(ba, Options())))
    #     @test true
    # end
end
