using bamg
using SparseArrays
using LinearAlgebra
using BALUtils

ba = readbal("test/5_47_generated.bbal")
bamg.bundleadjust(ba, solve=schur_cg_solver(ba, bamg_solver(ba, Options(coarse_size=1, num_levels=2))), verbose=true)
