using bamg
using PETScBinaryIO
using SparseArrays
using ArgParse
using BALUtils

s = ArgParseSettings(autofix_names = true)
@add_arg_table s begin
    "input_dir"
        help = "Bundle adjustment problem input directory"
        required = true
    "index"
        help = "Which matrix to use"
        required = true
        arg_type = Int
    "output"
        help = "Output filename"
        required = true
    "out_strength"
        required = true
end
for (ty, name) in zip(fieldtypes(Options), fieldnames(Options))
    add_arg_table(s, "--$name", Dict(:arg_type=>ty))
end
args = parse_args(ARGS, s)
x = filter(x->x[1] in fieldnames(Options) && x[2] != nothing, map(x -> (Symbol(x[1]), x[2]), collect(args)))
opts = Options(;x...)

d = args["input_dir"]
A = readpetsc(joinpath(d, "S.petsc"))
b = readpetsc(joinpath(d, "S-rhs.petsc"))
solution = readpetsc(joinpath(d, "solution.petsc"))
bal_file = joinpath(d, "problem_noised.bal")
bbal_file = joinpath(d, "problem_noised.bbal")
ba = if isfile(bal_file)
    readbal(bal_file)
else
    readbal(bbal_file)
end
dscale = map(x -> reshape(x[1:num_cameras(ba)*9], 9, :), readpetsc(joinpath(d, "dscale.petsc")))
poses = map(x -> reshape(x[1:num_cameras(ba)*9], 9, :), solution)

mg = create_multigrid(A[args["index"]], ba, dscale[args["index"]], poses[args["index"]], opts)
writepetsc(args["output"], mg.levels[1].P)
writepetsc(args["out_strength"], mg.levels[1].S)
