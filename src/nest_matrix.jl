"""
Matrix composed of submatrices. Submatrices can have different sizes and
different storage types (dense or sparse).
"""
struct NestMatrix{T, C <:AbstractMatrix{T}} <: AbstractMatrix{T}
    # TODO: allow mixing different kinds of matrix?
    blocks :: Matrix{C}
    size :: Tuple{Int,Int}
end

function NestMatrix(blks)
    for i in 1:size(blks, 1)
        if !all(size(blks[i,1],1) .== size.(blks[i,2:end],1))
            error("All blocks in row $i do not have the same size: $(size.(blks[i,:], 1))")
        end
    end
    for i in 1:size(blks, 2)
        if !all(size(blks[1,i],2) .== size.(blks[2:end,i],2))
            error("All blocks are not the same size")
        end
    end

    NestMatrix(blks, (sum(size.(blks[:,1], 1)), sum(size.(blks[1,:],2))))
end

function Base.eltype(A :: NestMatrix{T}) where T
    T
end
Base.size(A :: NestMatrix) = A.size

Base.copy(A :: NestMatrix) = NestMatrix(copy.(A.blocks), A.size)

Base.transpose(A :: NestMatrix) = Transpose(A)
Base.adjoint(A :: NestMatrix) = Adjoint(A)

SparseArrays.nnz(A :: NestMatrix) = sum(nnz.(A.blocks))

Base.IndexStyle(::Type{<:NestMatrix}) = IndexCartesian()
# TODO: make more efficient
function Base.getindex(A :: NestMatrix, i :: Int, j :: Int)
    @boundscheck checkbounds(A, i, j)
    for ((rx, ry), block) in iterblocks(A)
        if i in rx && j in ry
            return block[i-rx[1]+1, j-ry[1]+1]
        end
    end
end

struct NestIter
    A :: NestMatrix
end
function Base.iterate(A :: NestIter, state=1)
    blocks = A.A.blocks
    if state > length(blocks)
        nothing
    else
        ind = CartesianIndices(blocks)[state]
        x = sum(size.(blocks[1:ind[1]-1,ind[2]], 1))
        y = sum(size.(blocks[ind[1],1:ind[2]-1], 2))
        ((x + 1:x+size(blocks[state],1), y + 1:y+size(blocks[state],2)), blocks[state]), state+1
    end
end

iterblocks(A :: NestMatrix) = NestIter(A)

function Base.:*(A :: Adjoint{T, NestMatrix{T}}, B :: NestMatrix{T}) where T
    size(A.parent.blocks, 1) == size(B.blocks, 1) || DimensionMismatch()
    #TODO: better way to determine type?
    blks = Array{typeof(B.blocks[1])}(undef, size(A.parent.blocks, 2), size(B.blocks, 2))
    for i in 1:size(A.parent.blocks, 2)
        for j in 1:size(B.blocks, 2)
            blks[i,j] = A.parent.blocks[1, i]' * B.blocks[1, j]
            for k in 2:size(B.blocks, 1)
                blks[i,j] += A.parent.blocks[k, i]' * B.blocks[k, j]
            end
        end
    end
    NestMatrix(blks)
end

function LinearAlgebra.mul!(y :: AbstractVector, A :: NestMatrix{T,C}, x :: AbstractVector) where {T,C}
    size(A, 2) == size(x) || DimensionMismatch()
    y .= zero(T)
    for ((rx,ry), block) in iterblocks(A)
        y[rx] += block * x[ry]
    end
    y
end

function LinearAlgebra.mul!(y :: AbstractVector, A :: Adjoint{T, NestMatrix{T, C}}, x :: AbstractVector) where {T,C}
    size(A, 1) == size(x) || DimensionMismatch()
    y .= zero(T)
    for ((rx,ry), block) in iterblocks(A.parent)
        y[ry] += block' * x[rx]
    end
    y
end

function Base.show(io :: IO, A :: NestMatrix)
    print(io, "$(size(A,1))×$(size(A,2)) NestMatrix with blocks:")
    for ((rx,ry),block) in iterblocks(A)
        println(io, "$rx, $ry  =")
        show(io, block)
    end
end

function setdiag!(A :: NestMatrix, d :: AbstractVector)
    size(A, 2) == size(d) || DimensionMismatch()
    for ((rx, ry), block) in iterblocks(A)
        i = intersect(rx, ry)
        if length(i) > 0
            block[diagind(block)] = d[i]
        end
    end
    A
end

function SparseArrays.SparseMatrixCSC(A :: NestMatrix{T}) where {T}
    if length(A.blocks) == 1
        A.blocks[1]
    else
        is = Vector{Int}()
        sizehint!(is, reduce(+, nnz.(A.blocks)))
        js = Vector{Int}()
        sizehint!(js, reduce(+, nnz.(A.blocks)))
        ks = Vector{T}()
        sizehint!(ks, reduce(+, nnz.(A.blocks)))
        for ((rx, ry), block) in iterblocks(A)
            if typeof(block) <: AbstractSparseMatrix
                i, j, k = findnz(block)
                append!(is, rx[i])
                append!(js, ry[j])
                append!(ks, k)
            else
                for i in CartesianIndices(block)
                    push!(is, rx[i[1]])
                    push!(js, ry[i[2]])
                    push!(ks, block[i])
                end
            end
        end
        sparse(is, js, ks, size(A)...)
    end
end

"""
Matrix that is the product A'A + diagm(D) but does not compute the product until it is needed.
"""
mutable struct DelayedMatrixProduct{T,C<:AbstractMatrix{T}} <: AbstractMatrix{T}
    A :: C
    D :: Vector{T}
end

DelayedMatrixProduct(A) = DelayedMatrixProduct(A, zeros(eltype(A), size(A,2)))

Base.copy(A :: DelayedMatrixProduct) = DelayedMatrixProduct(copy(A.A), copy(A.D))

function Base.show(io :: IO, A :: DelayedMatrixProduct)
    println(io, "$(size(A, 1))×$(size(A, 2)) DelayedMatrixProduct (A'A+D) with A:")
    show(io, A.A)
    println(io, "D:")
    show(io, A.D)
end

function Base.:*(A :: DelayedMatrixProduct, x :: AbstractVector)
    size(A, 2) == size(x) || DimensionMismatch()
    A.A' * (A.A * x) + A.D .* x
end
Base.size(A :: DelayedMatrixProduct) = (size(A.A,2), size(A.A,2))

function squared_column_norm(A :: AbstractMatrix)
    Vector(mapslices(x->sum(x.^2), A; dims=1)[:])
end

function squared_column_norm(A :: NestMatrix)
    d = zeros(size(A,2))
    x = map(blk->mapreduce(x->x^2, +, blk, dims=1)[:], A.blocks)
    offs = cumsum(size.(x[1,:],1))
    for i in 1:size(x,2)
        v = reduce(+, x[:,i])
        d[offs[i] - size(v,1)+1:offs[i]] = v
    end
    d[:]
end

function LinearAlgebra.diag(A :: DelayedMatrixProduct)
    squared_column_norm(A.A) + A.D
end

"""
Scale A by l r so B = diagm(l) * A * diagm(r).
"""
function diagscale!(A :: NestMatrix, l, r)
    for ((rx, ry), block) in iterblocks(A)
        diagscale!(block, l == nothing ? nothing : l[rx], r == nothing ? nothing : r[ry])
    end
    A
end

function diagscale!(A :: DelayedMatrixProduct, l, r)
    if l == r
        diagscale!(A.A, nothing, r)
        A.D = l .* A.D .* r
        A
    else
        error("unimplemented")
    end
end

function diagscale!(A :: AbstractMatrix, l, r)
    if l != nothing
        lmul!(Diagonal(l), A)
    end
    if r != nothing
        rmul!(A, Diagonal(r))
    end
    A
end

"""
Updates D in A'A+D.
"""
function setdiag!(A :: DelayedMatrixProduct, d :: AbstractVector)
    A.D = d
    A
end

function SparseArrays.SparseMatrixCSC(A :: DelayedMatrixProduct)
    A_ = SparseMatrixCSC(A.A)
    B = A_'*A_
    B + Diagonal(A.D)
end

function Base.Matrix(A :: DelayedMatrixProduct)
    B = Matrix(A.A)'*Matrix(A.A)
    C = convert(Matrix, B)
    C + diagm(0 => A.D)
end

Base.convert(::Type{SparseMatrixCSC}, A :: DelayedMatrixProduct) = SparseMatrixCSC(A)

function LinearAlgebra.mul!(y :: AbstractVector, A :: DelayedMatrixProduct, x :: AbstractVector)
    tmp = A.A * x
    mul!(y, A.A', tmp)
end

SparseArrays.nnz(A :: DelayedMatrixProduct) = 2*nnz(A.A)
