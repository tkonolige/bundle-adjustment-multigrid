using LinearAlgebra
using SparseArrays
using Printf

mutable struct LM
    c         :: Float64
    c_initial :: Float64
    solution  :: Vector{Float64}
    jacobian  :: AbstractMatrix
    residuals :: Vector{Float64}
    scale     :: Vector{Float64}
    hessian   :: AbstractMatrix
    gradient  :: Vector{Float64}
    eta       :: Float64
    etamax    :: Float64
    i         :: Int
    d         :: Vector{Float64} # diagonal of hessian
    v         :: Float64
    damping   :: Float64
end

function LM()
    LM(0.0, 0.0, Vector{Float64}(), Matrix{Float64}(undef,0,0), Vector{Float64}(), Vector{Float64}(), Matrix{Float64}(undef,0,0), Vector{Float64}(), 0.0, 0.0, -1, Vector{Float64}(), 0.0, 0.0)
end

cost(residuals) = 0.5 * sum(residuals .^ 2.0)

function lm(f, jf, x :: Vector; maxiter=100, solve=cholesky_solver, steptol=1e-8, lazy_product=true, jacobian_scaling=true, verbose=false, rtol=1e-10, gradient_tol=1e-10, abstol=1e-10)
    lm = LM()
    lm.solution = copy(x)
    lm.residuals = f(lm.solution)
    lm.c = cost(lm.residuals)
    lm.c_initial = lm.c
    lm.jacobian = jf(lm.solution)
    lm.scale = ones(size(lm.jacobian, 2))
    if jacobian_scaling
        lm.scale = 1 ./ (1 .+ sqrt.(squared_column_norm(lm.jacobian)))
        diagscale!(lm.jacobian, nothing, lm.scale)
    end
    lm.hessian = lazy_product ? DelayedMatrixProduct(lm.jacobian) : lm.jacobian' * lm.jacobian
    lm.gradient = lm.jacobian' * lm.residuals

    # Eisenstat Walker convergence criteria
    lm.eta = 0.5 # Constant recommended by Eisenstat & Walker
    lm.etamax = 0.9

    # store diagonal of hessian and only update it when we take a step
    # use Vector to avoid keeping a sparse diagonal when it is dense
    lm.d = Vector(diag(lm.hessian))

    lm.v = 2.0
    lm.damping = 1e-4
    done = false
    lm.i = 1
    log = []
    push!(log, Dict("cost" => lm.c
                   , "damping" => lm.damping
                   , "v" => lm.v
                   , "gradient_norm" => norm(lm.gradient)
                   ))
    verbose && @printf " iter     cost   delta cost  ||step||  damping  ||gradient||   l iters     work  ls time  relnorm      eta\n"
    verbose && @printf "    0  %06.1e     % 06.1e   %06.1e  %06.1e       %06.1e      %4d  %7.2f  %06.1e  %06.1e  %06.1e\n" log[end]["cost"] 0 0 log[end]["damping"] log[end]["gradient_norm"] 0 0 0 0 lm.eta
    while lm.i <= maxiter && !done && norm(lm.gradient) > gradient_tol && lm.c > abstol
        if lazy_product
            setdiag!(lm.hessian, sqrt.(clamp.(lm.d, 1e-6, 1e32) * lm.damping))
        else
            lm.hessian[diagind(lm.hessian)] = lm.d + sqrt.(clamp.(lm.d, 1e-6, 1e32) * lm.damping)
        end
        stepdirection, solve_info = solve(lm.hessian, -lm.gradient, lm)
        if jacobian_scaling
            stepdirection .*= lm.scale
        end

        # store eta for printing
        eta = lm.eta

        c_ = lm.c
        if norm(stepdirection) < steptol * norm(lm.solution)
            done = true
        else
            solution_ = lm.solution .+ stepdirection

            # Could avoid this by using J*step + old_residuals = new_residuals
            residuals_ = f(solution_)
            c_ = cost(residuals_)

            if abs(lm.c - c_)/lm.c < rtol
                done = true
            end

            modelchange = 0.5 * stepdirection' * (stepdirection * lm.damping - lm.gradient)
            rho = (lm.c - c_) / modelchange



            # new_model_cost
            #  = 1/2 [f - J * step]^2
            #  = 1/2 [ f'f - 2f'J * step + step' * J' * J * step ]
            # model_cost_change
            #  = cost - new_model_cost
            #  = f'f/2  - 1/2 [ f'f - 2f'J * step + step' * J' * J * step]
            #  = f'J * step - step' * J' * J * step / 2
            #  = (J * step)'(f - J * step / 2)
            #  = (J * step)'(f - J * step / 2)
            Js = lm.jacobian * stepdirection
            m = -dot(Js, lm.residuals - Js/2)

            if lm.c > c_
                # good step
                # TODO: linesearch

                # Eisenstat Walker update condition
                # TODO: can we use the residual from the inner schur solve?
                lm.eta = abs(norm(residuals_) - norm(lm.residuals + lm.jacobian * (stepdirection ./ lm.scale))) / norm(lm.residuals)
                eta_ = eta^((1 + sqrt(5.0))/2)
                if eta_ > 0.1
                    lm.eta = max(lm.eta, eta_)
                end
                lm.eta = min(lm.eta, lm.etamax)

                lm.solution = solution_
                lm.residuals = residuals_
                lm.jacobian = jf(solution_)
                lm.c = c_
                if jacobian_scaling
                    lm.scale = 1 ./ (1 .+ sqrt.(squared_column_norm(lm.jacobian)))
                    diagscale!(lm.jacobian, nothing, lm.scale)
                end
                lm.hessian = lazy_product ? DelayedMatrixProduct(lm.jacobian) : lm.jacobian' * lm.jacobian
                lm.d = Vector(diag(lm.hessian))
                lm.gradient = lm.jacobian' * lm.residuals

                lm.damping = lm.damping * max(1/3, 1 - (2 * rho - 1)^3)
                lm.v = 2.0
            else
                lm.damping = lm.damping * lm.v
                lm.v *= 2
            end
        end

        push!(log, Dict("cost" => lm.c
                       , "delta_cost" => log[end]["cost"] - c_
                       , "damping" => lm.damping
                       , "v" => lm.v
                       , "gradient_norm" => norm(lm.gradient)
                       , "solve_info" => solve_info
                       , "step_norm" => norm(stepdirection)
                       , "eta" => eta
                       ))

        verbose && @printf "   %2d  %06.1e     % 06.1e   %06.1e  %06.1e       %06.1e      %4d  %7.2f  %06.1e  %06.1e  %06.1e\n" lm.i c_ log[end]["delta_cost"] log[end]["step_norm"] log[end]["damping"] log[end]["gradient_norm"] get(log[end]["solve_info"], "iters", 1) get(log[end]["solve_info"], "work", 0) get(log[end]["solve_info"], "solve_time", 0) get(log[end]["solve_info"], "relnorm", 0) log[end]["eta"]
        lm.i += 1
    end

    lm.solution, log
end

include("solvers.jl")

