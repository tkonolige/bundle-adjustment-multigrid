include("nest_matrix.jl")
include("bcoo.jl")
include("bsc.jl")
using InteractiveUtils

export lu_solver, cholesky_solver, schur_solver, schur_cg_solver, pbjacobi

function lu_solver(A :: SparseMatrixCSC, b, _)
    x, time = @timed A \ b
    x, Dict("solve_time"=>time)
end

function lu_solver(A :: DelayedMatrixProduct, b, lm)
    lu_solver(SparseMatrixCSC(A), b, lm)
end

function cholesky_solver(A :: SparseMatrixCSC, b, _)
    x, time = @timed cholesky(Symmetric(A)) \ b
    x, Dict("solve_time"=>time)
end

function cholesky_solver(A :: DelayedMatrixProduct, b :: Vector, lm :: LM)
    cholesky_solver(SparseMatrixCSC(A), b, lm)
end

mutable struct SchurComplement{I}
    finds :: UnitRange{Int}
    einds :: UnitRange{Int}
    S :: Union{Nothing,SparseMatrixBSC{I,I,Float64,Int}}
end

function SchurComplement(finds :: UnitRange{Int}, einds :: UnitRange{Int}, bs)
    SchurComplement{bs}(finds, einds, nothing)
end

struct Chunks{I,J,V,T}
    E :: SparseMatrixBCOO{I,J,V,T}
end
function Base.iterate(c :: Chunks, state=(1,1))
    (i, row_off) = state
    i > size(c.E,2) ÷ bs(c.E,2) && return nothing
    # find the extent of this chunk
    current_row = c.E.coords[row_off][2]
    chunk_end = findlast(x -> x[2] == current_row, @view(c.E.coords[row_off:end])) + row_off - 1
    chunk = row_off:chunk_end
    ((i, chunk), (i+1, chunk_end+1))
end

function complement(s :: SchurComplement, A :: SparseMatrixCSC, b :: Vector, r :: Vector)
    # convert D^-1 to SparseMatrixCSC for performance. BlockDiagonalMatrix
    # does this under the hood for each multiply.
    points_inv = SparseMatrixCSC(inv(block_diag(A[s.einds, s.einds], 3)))
    S = A[s.finds, s.finds] - A[s.finds, s.einds] * points_inv * A[s.einds, s.finds]
    rhs = b[s.finds] - A[s.finds, s.einds] * points_inv * b[s.einds]
    S, rhs
end
function back_substitute(s :: SchurComplement, A :: SparseMatrixCSC, b :: Vector, x :: Vector, r :: Vector)
    points_inv = SparseMatrixCSC(inv(block_diag(A[s.einds, s.einds], 3)))
    y = points_inv * (b[s.einds] - A[s.einds, s.finds] * x)
    vcat(x,y)
end

function complement(s :: SchurComplement, A :: DelayedMatrixProduct, b :: Vector, r :: Vector)
    # assume A has the correct layout: camera block on the left, point block on the right
    F = A.A.blocks[1,1] :: SparseMatrixBCOO
    E = A.A.blocks[1,2] :: SparseMatrixBCOO
    complement(s, F, E, A.D, r)
end

function form_matrix(s :: SchurComplement, F :: SparseMatrixBCOO{I,J,V,T}, E :: SparseMatrixBCOO{I,K,V,T}) where {I,J,K,V,T}
    chunk_sizes = 0
    for (_, c) in Chunks(E)
        chunk_sizes += length(c)^2 * bs(F,2)^2
    end
    num_nonzeros = bs(F,2)^2 * nblocks(F) + size(F,2) + chunk_sizes # TODO: slightly off

    is = Vector{Int}()
    sizehint!(is, num_nonzeros)
    js = Vector{Int}()
    sizehint!(js, num_nonzeros)

    @inbounds for (i, chunk) in Chunks(E)
        for k in chunk
            for j in chunk
                x = F.coords[k][2]
                y = F.coords[j][2]
                push!(is, x ÷ bs(F,2) + 1)
                push!(js, y ÷ bs(F,2) + 1)
            end
        end
    end

    # Add diagonal
    @inbounds for i in 1:size(F,2)÷bs(F,2)
        push!(is, i)
        push!(js, i)
    end

    SparseMatrixBSC(is, js, bs(F,2), bs(F,2), size(F,2), size(F,2))
end

function complement(s :: SchurComplement, F :: SparseMatrixBCOO{I,J,V,T}, E :: SparseMatrixBCOO{I,K,V,T}, D :: Vector{V}, r :: Vector{V}) where {I,J,K,V,T}
    if s.S isa Nothing
        s.S = form_matrix(s, F, E)
    end
    zero!(s.S)

    # S = F'F + Diagonal(D[s.finds])
    s.S += block_diag(F)
    s.S += Diagonal(D[s.finds])

    EE = block_diag(E)
    add!(EE, Diagonal(D[s.einds]))
    ETEi = inv(EE)
    # rhs_ = -(F' * (r - E * (ETEi * (E' * r))))
    rhs = - (F' * r)
    ETEi_b = ETEi * (E' * r)
    B = zero(MMatrix{J,J,V})
    FtE = zero(MMatrix{J,K,V})
    @inbounds for (i, chunk) in Chunks(E)
        # S -= F' * E * ETEi.blks[:, :, i] * E'*F
        for k in chunk
            @assert E.coords[k][1] == F.coords[k][1]
            FtE .= F.blocks[k]'*E.blocks[k]

            # rhs -= F'E * (E'E)^-1 b
            inds = (i-1)*3+1:i*3
            rhs[F.coords[k][2]:F.coords[k][2]+bs(F,2)-1] += FtE * ETEi_b[inds]

            # TODO: could store all F'E and E'F block
            # TODO: could compute only half given symmetry
            for j in chunk
                B .= -(FtE * (ETEi.blocks[i] * E.blocks[j]')) * F.blocks[j]
                x = F.coords[k][2]
                y = F.coords[j][2]
                s.S.mat[x ÷ bs(F,2) + 1, y ÷ bs(F,2) + 1] += B
            end
        end
    end

    SparseMatrixCSC(s.S), rhs
end
# E'E y + E'Fz = b
# y = (E'E)^-1 * (b - E'Fz)
function back_substitute(s :: SchurComplement, A :: DelayedMatrixProduct, b :: AbstractVector, x :: AbstractVector, r :: Vector)
    F = A.A.blocks[1,1]
    E = A.A.blocks[1,2]
    size(A,2) == length(b) || DimensionMismatch()
    size(F,2) == length(x) || DimensionMismatch()

    # TODO: store this
    EE = block_diag(E)
    add!(EE, Diagonal(A.D[s.einds]))
    ETEi = inv(EE)
    # y = zeros(size(E,2))
    # @inbounds for (i, chunk) in Chunks(E)
    #     inds = (i-1)*3+1:i*3
    #     for j in chunk
    #         y[E.coords[j][2]:E.coords[j][2]+bs(E,2)-1] += E.blocks[j]' * (F.blocks[j] * x[F.coords[j][2]:F.coords[j][2]+bs(F,2)-1])
    #     end
    # end
    # y = ETEi * (-(E' * r) - y)
    y = ETEi * (E' * (-r - F * x))
    vcat(x,y)
end
function (s::SchurComplement)(A :: AbstractMatrix, b :: Vector, lm :: LM; inner_solve=cholesky_solver)
    (S, rhs), complement_time = @timed complement(s, A, b, lm.residuals)
    x, solve_info = inner_solve(S, rhs, lm)
    y, backsub_time = @timed back_substitute(s, A, b, x, lm.residuals)
    y, merge(solve_info, Dict("schur_time" => complement_time + backsub_time, "schur_nnz" => nnz(S), "schur_size" => size(S,1)))
end

"""
Solver that eliminates the point block from J'J in a bundle adjust. Greatly
reduces the size of the system to solve. `inner_solve` gets the reduced camera
system.

Ax + By = a    A is the camera-camera block
Cx + Dy = b    D is the point-point block

(A - B D^-1 C) x = a - B D^-1 b    Inner solve
y = D^-1 (b - C x)                 Recover y
"""
function schur_solver(ba :: BA, inner_solve=lu_solver)
    camera_inds = 1:num_cameras(ba) * 9
    point_inds = num_cameras(ba)*9+1:num_cameras(ba)*9+num_points(ba)*3
    schur = SchurComplement(camera_inds, point_inds, 9)
    function solve(A :: DelayedMatrixProduct, b :: Vector, lm :: LM, inner_solve=inner_solve)
        schur(A, b, lm, inner_solve=inner_solve)
    end
end

"""
Convergence function that terminates based on progress in the nonlinear least-squares problem.

Nash, Stephen G., and Ariela Sofer. “Assessing a Search Direction within a Truncated-Newton Method.” Operations Research Letters 9, no. 4 (July 1990): 219–21. https://doi.org/10.1016/0167-6377(90)90065-D.
"""
function nash_converged(tol)
    Qn = -1.0
    function converged(it, iteration)
        Qn1 = -0.5*dot(it.x, it.b + it.r)
        # @show Qn1
        conv = (iteration > 1 && iteration * (Qn1 - Qn)/Qn1 <= tol) || iteration >= it.maxiter
        Qn = Qn1
        conv
    end
end

"""
Same termination condition as `nash_converged` but operates on the reduced
system of a schur complement.

See pg40 in notebook
"""
function schur_nash_converged(F :: SparseMatrixBCOO, E :: SparseMatrixBCOO, D_point :: Vector, b_point :: Vector, tol :: Float64, alt = false, log=nothing)
    Qn = 0.0
    Dinv = inv(add!(block_diag(E), Diagonal(D_point)))
    bDb = dot(b_point, Dinv * b_point)
    CDb = F' * (E * (Dinv * b_point))
    function converged(it, iteration)
        Qn1 = -0.5 * (dot(it.x, it.b + it.r) + bDb - dot(it.x, CDb))
        if !(log == nothing)
            push!(log, Qn1)
        end
        # @show Qn1, Qn
        # println("rnorm: $(it.residual) nash: $(iteration * (Qn1 - Qn)/Qn1)")
        # adding the absolute value to the iteration test seems to give the
        # largest benefits in convergence time
        conv = if alt
            (iteration > 1 && sqrt(abs(Qn - Qn1)) <= tol) || iteration >= it.maxiter
        else
            (iteration > 1 && iteration * abs((Qn1 - Qn)/Qn1) <= tol) || iteration >= it.maxiter
        end
        Qn = Qn1
        conv
    end
end

function gtol_converged(gtol :: Float64)
    function converged(it, iteration)
        iteration > 1 && (iteration >= it.maxiter || gtol >= it.residual)
    end
end

struct ImplicitSchurComplement
    E :: SparseMatrixCSC
    F :: SparseMatrixCSC
    FD :: Diagonal
    ETEi :: BlockDiagonalMatrix
end

function Base.eltype(A :: ImplicitSchurComplement)
    eltype(A.E)
end

function ImplicitSchurComplement(A :: DelayedMatrixProduct)
    E = A.A.blocks[1,2]
    F = A.A.blocks[1,1]
    ETEi = inv(add!(block_diag(E), Diagonal(A.D[size(F,2)+1:size(F,2)+size(E,2)])))
    ImplicitSchurComplement(SparseMatrixCSC(E), SparseMatrixCSC(F), Diagonal(A.D[1:size(F,2)]), ETEi)
end

function LinearAlgebra.mul!(y :: Vector{Float64}, A :: ImplicitSchurComplement, x :: Vector{Float64})
    Fx = A.F * x
    y .= A.FD * x + A.F' * (Fx - A.E * (A.ETEi * (A.E' * Fx)))
end

function Base.size(A :: ImplicitSchurComplement)
    (size(A.F,2), size(A.F,2))
end

function Base.size(A :: ImplicitSchurComplement, i :: Int)
    Base.size(A)[i]
end

function SparseArrays.nnz(A :: ImplicitSchurComplement)
    nnz(A.F) * 4 + nnz(A.E) * 2 + nnz(A.ETEi) + size(A.F,2)
end

function cg_solver(;kwargs...)
    function solve(A, b, lm)
        # gtol is set for the Eisenstat Walker termination criteria
        # TODO: this seems kinda ugly, shouldn't it be passed in?
        (x, info), time = @timed cg(A, b; log=true, kwargs...)
        flops_P = if :Pl in keys(Dict(kwargs))
            flops(Dict(kwargs)[:Pl])
        else
            0
        end
        flops_cg = niters(info) * (nnz(A)+flops_P)
        work_cg = flops_cg / nnz(A)
        relnorm = (length(info[:resnorm]) > 0) ? info[:resnorm][end]/info[:resnorm][1] : norm(b - A * x)
        x, merge(Dict(info), Dict( "solve_time"=>time
                                 , "flops"=>flops_cg
                                 , "work"=>work_cg
                                 , "relnorm" => relnorm
                                 , "rnorm" => info[:resnorm][end]
                                 ))
    end
end

function schur_cg_solver(ba :: BA, prec; convergence_test="nash", tol=0.1)
    schur = schur_solver(ba)
    function solve(A :: DelayedMatrixProduct, b :: Vector, lm :: LM)
        E = A.A.blocks[1,2]
        F = A.A.blocks[1,1]
        c = 1:num_cameras(ba) * 9
        p = num_cameras(ba)*9+1:num_cameras(ba)*9+num_points(ba)*3
        clog = Vector{Float64}()

        converged = if convergence_test == "nash"
            schur_nash_converged(F, E, A.D[p], b[p], tol, false, clog)
        elseif convergence_test == "nashalt"
            schur_nash_converged(F, E, A.D[p], b[p], tol, true)
        elseif convergence_test == "purenash"
            nash_converged(tol)
        elseif convergence_test == "rtol"
            default_converged(tol)
        elseif convergence_test == "eisenstat"
            gtol_converged(lm.eta * norm(lm.residuals))
        else
            error("Unknown convergence_test $convergence_test")
        end

        A_ = ImplicitSchurComplement(A)
        # TODO: add in schur flops
        function inner_solve(A, b, lm)
            Pl, setup_info = prec(A, b, lm)
            x, info = cg_solver(Pl=Pl, converged=converged)(A_, b, lm)
            clog_ = copy(clog)
            empty!(clog)
            x, merge(info, setup_info, Dict("Q"=>clog_))
        end
        schur(A, b, lm, inner_solve)
    end
end

function pbjacobi(A :: SparseMatrixCSC, b :: Vector, lm :: LM)
    P, time = @timed block_diag(A, 9) # TODO: FIXME: hard coded block size
    P, Dict("setup_time" => time)
end

function Base.Dict(c :: ConvergenceHistory)
    Dict("iters" => c.iters, "residuals" => c[:resnorm])
end
