include("block_diag.jl")

"""
Block sparse matrix in coordinate format.

`coords` is a vector of the starting offset of each block. It is sorted by rows then columns.
"""
# TODO: might be worthwhile to use BSR or BSC
# TODO: this is really not bcoo or bsr or bsc, it has only one block per row
struct SparseMatrixBCOO{I,J,Tv,Ti,L} <: AbstractSparseMatrix{Tv,Ti}
    coords :: Vector{Tuple{Ti,Ti}}
    blocks :: Vector{SMatrix{I,J,Tv,L}}
    size :: Tuple{Ti,Ti}
end

bs(A :: SparseMatrixBCOO{I,J}) where {I,J} = (I,J)
bs(A :: SparseMatrixBCOO, i :: Int) = bs(A)[i]
nblocks(A :: SparseMatrixBCOO) = length(A.blocks)
function blockinds(A :: SparseMatrixBCOO, i :: Int)
    (ix, iy) = A.coords[i]
    (ix:ix+bs(A,1)-1, iy:iy+bs(A,2)-1)
end

function SparseMatrixBCOO(x :: Int, y :: Int, bs :: Tuple{Int,Int}, nblocks :: Int)
    SparseMatrixBCOO{bs[1],bs[2],Float64,Int,bs[1]*bs[2]}(fill((-1,-1), nblocks), fill(zeros(SMatrix{bs[1],bs[2]}), nblocks), (x,y))
end

Base.size(A :: SparseMatrixBCOO) = A.size
Base.copy(A :: SparseMatrixBCOO) = SparseMatrixBCOO(copy(A.coords), copy.(A.blocks), A.size)

Base.iterate(A :: SparseMatrixBCOO, state=1) = (state > nblocks(A)) ? nothing : ((blockinds(A, state), @inbounds A.blocks[state]), state+1)

function SparseArrays.SparseMatrixCSC(A :: SparseMatrixBCOO{I,J,Tv,Ti}) :: SparseMatrixCSC{Tv,Ti} where {I,J,Tv,Ti}
    num_entries = length(A.coords) * bs(A,1) * bs(A,2)
    is = Vector{Ti}()
    sizehint!(is, num_entries)
    js = Vector{Ti}()
    sizehint!(js, num_entries)
    ks = Vector{Tv}()
    sizehint!(ks, num_entries)

    for ((rx,ry), block) in A
        for i in rx
            for j in ry
                push!(is, i)
                push!(js, j)
                push!(ks, block[i-rx[1]+1,j-ry[1]+1])
            end
        end
    end

    sparse(is, js, ks, size(A)...)
end

"""
Assumes A' and B have one nonzero per row.
"""
function LinearAlgebra.mul!(C :: SparseMatrixBCOO{I,K,T,Ti,L}, A :: Adjoint{T,SparseMatrixBCOO{I,J,T,Ti,L}}, B :: SparseMatrixBCOO{J,K,T,Ti,L}) where {I,J,K,T,Ti,L}
    @assert nblocks(A.parent) == nblocks(B)

    blocks = zeros(bs(A.parent,2), bs(B,2), nblocks(B))
    coords = fill((-1,-1), nblocks(B))

    for (i, (((Ax, Ay), Ablock), ((Bx, By), Bblock))) in enumerate(zip(A.parent, B))
        coords[i] = (Ay, By)
        blocks[i] = Ablock' * Bblock
    end

    p = sortperm(coords)
    SparseMatrixBCOO(coords[p], blocks[:,:,p], (size(A.parent,2), size(B,2)))
end

function LinearAlgebra.mul!(y :: AbstractVector{T}, A :: Adjoint{T,SparseMatrixBCOO{I,J,T,Ti,L}}, x :: AbstractVector{T}) where {I,J,T,Ti,L}
    size(A,2) == length(x) || DimensionMismatch()
    size(A,1) == length(y) || DimensionMismatch()
    y .= zero(T)
    rx_ = iterate(A.parent)[1][1][1]
    x_ = @inbounds x[rx_]
    @inbounds for ((rx, ry), block) in A.parent
        if rx_ != rx
            rx_ = rx
            x_ = x[rx]
        end
        y[ry] .+= block' * x_
    end
    y
end

function LinearAlgebra.mul!(y :: AbstractVector{T}, A :: SparseMatrixBCOO{I,J,T,Ti,L}, x :: AbstractVector{T}) where {I,J,T,Ti,L}
    size(A,2) == length(x) || DimensionMismatch()
    size(A,1) == length(y) || DimensionMismatch()
    @inbounds for ((rx, ry), block) in A
        y[rx] = block * x[ry]
    end
    y
end

"""
Construct a block diagonal matrix from A'A where A is a block matrix with 1
nonzero block per row.
"""
function block_diag(A :: SparseMatrixBCOO{I,J,V,T,L}) where {I,J,V,T<:Integer,L}
    blocks = fill(zeros(SMatrix{J,J,V}), size(A, 2)÷bs(A,2))

    @inbounds for (i, (j, k)) in enumerate(A.coords)
        B = A.blocks[i]
        blocks[k ÷ bs(A,2) + 1] += B' * B
    end

    BlockDiagonalMatrix{J,V,T}(size(A,2), size(A,2), blocks)
end

SparseArrays.nnz(A :: SparseMatrixBCOO) = reduce(*, [bs(A)..., length(A.blocks)])

function SparseArrays.findnz(A :: SparseMatrixBCOO{Tv,Ti}) where {Tv,Ti}
    findnz(SparseMatrixCSC(A))
end

function Base._mapreducedim!(f, op, R::AbstractArray, A::SparseMatrixBCOO{I,J,Tv,Ti,L}) where {I,J,Tv,Ti,L}
    if size(R, 1) == 1
        size(R,2) == size(A,2) || DimensionMismatch()
        @inbounds for ((rx, ry), block) in A
            R[1,ry] = mapreduce(f, op, block, dims=1)
        end
        R
    else
        error("unimplemented")
    end
end

function LinearAlgebra.rmul!(A :: SparseMatrixBCOO, D :: Diagonal)
    for (i, ((rx, ry), block)) in enumerate(A)
        A.blocks[i] = block * Diagonal(D.diag[ry])
    end
    A
end

function Base.getindex(A :: SparseMatrixBCOO{I,J,T}, i :: Int, j :: Int) where {I,J,T}
    ind = findfirst(x -> x[1] <= i && x[1] + bs(A,1) > i && x[2] <= j && x[2] + bs(A,2) > j, A.coords)
    ind == nothing ? zero(T) : A.blocks[ind][i - A.coords[ind][1] + 1, j - A.coords[ind][2] + 1]
end
