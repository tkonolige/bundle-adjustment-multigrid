using LinearAlgebra
using SparseArrays
using BALUtils
using Rotations
import ForwardDiff
using Random
using Libdl

# julia _still_ doesn't have a proper module system
include("lm.jl")

export bundleadjust

# function __init__()
#     dlopen("../ba-tao/build/libderivatives.dylib")
# end
#
# function ceres_derivative(observed, camera, point)
#     params = vcat(observed, camera, point)
#     residuals = zeros(2)
#     jacobian = zeros(24)
#     ccall(:compute_derivatives, Int, (Ptr{Float64}, Ptr{Float64}, Ptr{Float64}), params, residuals, jacobian)
#     camera_jac = transpose(reshape(jacobian[1:9*2], 9, 2))
#     point_jac = transpose(reshape(jacobian[9*2+1:end], 3, 2))
#     residuals, camera_jac, point_jac
# end

function angle_axis_rotate(rot, p)
    theta2 = dot(rot, rot)
    if theta2 > eps(Float64)
        theta = sqrt(theta2)
        w = rot / theta
        p * cos(theta) + cross(w, p) * sin(theta) + w * dot(w, p) * (1 - cos(theta))
    else
        p + cross(rot, p)
    end
end

function project(x)
    camera = x[1:9]
    point = x[10:12]
    p = angle_axis_rotate(camera[1:3], point) + camera[4:6]
    xp = -p[1]/p[3]
    yp = -p[2]/p[3]
    l1 = camera[8]
    l2 = camera[9]
    r2 = xp^2.0 + yp^2.0
    distortion = 1.0 + r2 * (l1 + l2 * r2)
    focal = camera[7]
    predicted_x = focal * distortion * xp
    predicted_y = focal * distortion * yp
    [predicted_x, predicted_y]
end

function generate_problem(ba :: BA, sparsify=false)
    initial_solution = zeros(num_cameras(ba)*9+num_points(ba)*3)
    for (i, camera) in enumerate(ba.cameras)
        initial_solution[(i-1)*9+1:i*9] = vec(camera)
    end
    for (i, point) in enumerate(ba.points)
        initial_solution[num_cameras(ba)*9 + (i-1)*3+1:num_cameras(ba)*9+i*3] = vec(point)
    end

    # switch visibility graph from camera->points to point->cameras
    pvis = map(x -> Vector{Tuple{Int,Float64,Float64}}(), 1:num_points(ba))
    for (ic, obs) in enumerate(ba.observations)
        for (ip, x, y) in obs
            push!(pvis[ip], (ic, x, y))
        end
    end

    function residuals(solution :: Vector{Float64})
        rs = Vector{Float64}()
        sizehint!(rs, num_observations(ba))
        for (ip, obs) in enumerate(pvis)
            for (ic, x, y) in obs
                camera = solution[(ic-1) * 9+1:ic*9]
                point = solution[num_cameras(ba) * 9 + (ip - 1) * 3+1:num_cameras(ba) * 9 + ip*3]
                p = project(vcat(camera, point))
                push!(rs, x - p[1])
                push!(rs, y - p[2])

                # residuals, _, _ = ceres_derivative([x, y], camera, point)
                # push!(rs, -residuals[1])
                # push!(rs, -residuals[2])
            end
        end
        rs
    end

    # cache autodiff of project for faster jacobian evaluation
    tmp = zeros(Float64, 12)
    cfg = ForwardDiff.JacobianConfig(project, tmp, ForwardDiff.Chunk{12}())
    function jacobian(solution :: Vector{Float64})
        Jpoints = SparseMatrixBCOO(num_observations(ba) * 2, num_points(ba) * 3, (2,3), num_observations(ba))
        Jcameras = SparseMatrixBCOO(num_observations(ba) * 2, num_cameras(ba) * 9, (2,9), num_observations(ba))

        # mapping from camera id to residual blocks in its column
        # TODO: compute once
        vis_blocks = map(x -> Vector{Int}(), 1:num_cameras(ba))

        obs_off = 1
        for (ip, obs) in enumerate(pvis)
            for (ic, x, y) in obs
                camera = solution[(ic-1) * 9+1:ic*9]
                point = solution[num_cameras(ba) * 9 + (ip - 1) * 3+1:num_cameras(ba) * 9 + ip*3]
                # Need the negative because projected points are subtracted from observed
                jac = - ForwardDiff.jacobian(project, vcat(camera, point), cfg)

                Jpoints.coords[obs_off] = ((obs_off-1)*2+1, (ip-1)*3+1)
                Jcameras.coords[obs_off] = ((obs_off-1)*2+1, (ic-1)*9+1)

                Jpoints.blocks[obs_off] = jac[:,10:12]
                Jcameras.blocks[obs_off] = jac[:,1:9]

                # _, cjac, pjac = ceres_derivative([x, y], camera, point)
                # Jpoints.blocks[obs_off] = -pjac
                # Jcameras.blocks[obs_off] = -cjac

                push!(vis_blocks[ic], obs_off)

                obs_off += 1
            end
        end

        if sparsify
            inds = map(vis_blocks) do blocks
                # hcat of static arrays is slow
                Jc_block = vcat(Matrix.(Jcameras.blocks[blocks])...)
                # Jc_block_approx = interpolative_decomposition(Jc_block)
                # for (i, b) in enumerate(blocks)
                #     Jcameras.blocks[b] .= Jc_block_approx[(i-1)*2+1:i*2,:]
                # end
                inds = interpolative_decomposition(Jc_block)
                # @show blocks
                blocks[inds]
            end |> x -> vcat(x...)
            # @show inds
            Jcameras = SparseMatrixBCOO(Jcameras.coords[inds], Jcameras.blocks[inds], Jcameras.size)
        end

        NestMatrix(reshape([Jcameras, Jpoints], 1, 2))
    end
    residuals, jacobian, initial_solution
end

function bundleadjust(ba :: BA; kwargs...)
    residuals, jacobian, initial_solution = generate_problem(ba)
    solution, log = lm(residuals, jacobian, initial_solution; kwargs...)
    cameras = map(1:num_cameras(ba)) do i
        Camera(solution[(i-1)*9+1:i*9])
    end
    points = map(1:num_points(ba)) do i
        solution[num_cameras(ba)*9+(i-1)*3+1:num_cameras(ba)*9+i*3]
    end
    BA(cameras, ba.observations, points), log
end

function interpolative_decomposition2(Jc :: Matrix; width = 0.00001)
    Jc_inv = transpose(Jc)

    F = svd(Jc_inv, full=true)
    S = log2.(F.S)
    @show length(S)
    inds = []
    last_val = 1000000000
    for i in 1:length(S)
        if last_val - S[i] > width
            last_val = S[i]
            push!(inds, i)
        end
    end

    inds
end

function interpolative_decomposition(Jc :: Matrix; width = 1.0)
    Jc_inv = transpose(Jc)

    sli = pyimport("scipy.linalg.interpolative")
    k, idx, proj = sli.interp_decomp(Jc, 1e-8)
    # B = sli.reconstruct_skel_matrix(Jc_inv, k, idx)
    # Jci_approx = sli.reconstruct_matrix_from_id(B, idx, proj)
    #
    # transpose(Jci_approx)
    # @show length(idx), size(Jc)
    @show k
    unique((idx[1:k] .÷ 2) .+ 1)
    sort(randperm(size(Jc,1)÷2)[1:end-1])

end
