using LinearAlgebra
using SparseArrays
using StaticArrays

"""
Matrix storing only the block diagonal. All blocks in the diagonal are the same size.
"""
struct BlockDiagonalMatrix{I,F,Ti} <: AbstractSparseMatrix{F,Ti}
    m :: Ti
    n :: Ti
    blocks :: Vector{MMatrix{I,I,F}}
end

function bs(A :: BlockDiagonalMatrix{I,F,T}) where {I,F,T}
    I
end
Base.size(A :: BlockDiagonalMatrix) = (A.m, A.n)
Base.length(A :: BlockDiagonalMatrix) = length(A.blocks)
function Base.copy(A :: BlockDiagonalMatrix{J,V,I}) where {J,V,I}
    BlockDiagonalMatrix{J,V,I}(A.m, A.n, copy(A.blocks))
end
function Base.eltype(A :: BlockDiagonalMatrix{I,V}) where {I,V}
    V
end

function Base.show(io :: IO, A :: BlockDiagonalMatrix)
    println("$(size(A, 1))×$(size(A,2)) $(typeof(A)) with $(bs(A))×$(bs(A)) blocks:")
    Base.show(io, A.blocks)
end

function LinearAlgebra.ldiv!(x :: AbstractVector, A :: BlockDiagonalMatrix, b :: AbstractVector)
    for ((i, j), block) in A
        x[i] = block \ b[j]
    end
    x
end

function LinearAlgebra.:\(A :: BlockDiagonalMatrix, b :: AbstractVector)
    x = similar(b)
    ldiv!(x, A, b)
    x
end

function Base.iterate(A :: BlockDiagonalMatrix, state=1)
    r = (state-1)*bs(A)+1:state*bs(A)
    state > length(A.blocks) ? nothing : (((r,r), A.blocks[state]), state+1)
end

# TODO: can this use linear indexing?
Base.IndexStyle(::Type{<:BlockDiagonalMatrix}) = IndexCartesian()

"""
Compute which block and the index within that block from a general index.

For example:
    bs: 3, i: 7
    bindex(A, i) = 3, 1
"""
@inline function bindex(A :: BlockDiagonalMatrix, i :: Integer)
    (i - 1) ÷ bs(A) + 1, (i - 1) % bs(A) + 1
end

@inline function Base.getindex(A :: BlockDiagonalMatrix, i :: Integer, j :: Integer)
    ib, ii = bindex(A, i)
    jb, jj = bindex(A, j)
    abs(i - j) >= bs(A) || ib != jb ? zero(eltype(A)) : A.blocks[ib][ii, jj]
end

@inline function Base.setindex!(A :: BlockDiagonalMatrix{J,T,I}, v :: T, i :: Integer, j :: Integer) where {J,T,I}
    ib, ii = bindex(A, i)
    jb, jj = bindex(A, j)
    @boundscheck abs(i - j) >= bs(A) || ib != jb || BoundsError()
    A.blocks[ib][ii, jj] = v
end

function LinearAlgebra.mul!(y :: AbstractVector, A :: BlockDiagonalMatrix, x :: AbstractVector)
    size(A,2) == length(x) || DimensionMismatch()
    size(A,1) == length(y) || DimensionMismatch()
    @inbounds for ((i, j), block) in A
        y[i] = block * x[j]
    end
    y
end

Base.:*(A :: BlockDiagonalMatrix, y :: SparseMatrixCSC) = SparseMatrixCSC(A) * y
Base.:*(y :: SparseMatrixCSC, A :: BlockDiagonalMatrix) = y * SparseMatrixCSC(A)
function add!(A :: BlockDiagonalMatrix{J,T,I}, D :: Diagonal{T}) :: BlockDiagonalMatrix{J,T,I} where {J,T,I}
    size(A) == size(D) || DimensionMismatch()
    @inbounds for i in 1:size(D,1)
        A[i,i] += D.diag[i]
    end
    A
end

function _inv(A :: BlockDiagonalMatrix{J,T,I}) where {J, T, I}
    BlockDiagonalMatrix{J,T,I}(A.m, A.n, inv.(A.blocks))
end

function Base.inv(A :: BlockDiagonalMatrix)
    _inv(A)
end

function Base.eltype(::Type{BlockDiagonalMatrix{J,F,I}}) where {J,F,I}
    F
end

SparseArrays.nnz(A :: BlockDiagonalMatrix) = reduce(*, [bs(A)..., length(A.blocks)])

function Base.convert(::Type{SparseMatrixCSC}, A :: BlockDiagonalMatrix)
    is, js, ks = findnz(A)
    sparse(is, js, ks, size(A)...)
end

SparseArrays.sparse(A :: BlockDiagonalMatrix) = convert(SparseMatrixCSC, A)
SparseArrays.SparseMatrixCSC(A :: BlockDiagonalMatrix) = sparse(A)

Base.Matrix(A :: BlockDiagonalMatrix) = convert(Matrix, A)
function Base.convert(::Type{Matrix}, A :: BlockDiagonalMatrix)
    B = zeros(size(A))
    for ((i,j), block) in A
        B[i,j] = block
    end
    B
end

"""
Extract the block diagonal from a matrix. `bs` determines the size of each block.
"""
function block_diag(A :: Union{SparseMatrixCSC, Matrix}, bs :: Int)
    size(A,1) == size(A,2) || error("Square matrices only")
    size(A,1) % bs == 0 || error("Block size $bs does not evenly divide matrix size $(size(A,1))")
    blocks = Vector{SMatrix{bs,bs,eltype(A)}}(undef, size(A, 1)÷bs)
    @inbounds for i in 1:bs:size(A,1)
        blocks[(i-1)÷bs+1] = A[i:(i+bs-1), i:(i+bs-1)]
    end
    BlockDiagonalMatrix{bs,eltype(A),Int}(size(A)..., blocks)
end

function SparseArrays.findnz(A :: BlockDiagonalMatrix{J,T,I}) where {J,T,I}
    is = Vector{I}()
    sizehint!(is, nnz(A))
    js = Vector{I}()
    sizehint!(js, nnz(A))
    ks = Vector{T}()
    sizehint!(ks, nnz(A))

    @inbounds for ((r, _), block) in A
        for i in r
            for j in r
                push!(is, i)
                push!(js, j)
                push!(ks, block[i-r[1]+1, j-r[1]+1])
            end
        end
    end

    is, js, ks
end

flops(A :: BlockDiagonalMatrix) = nnz(A)

struct InvertedBlockDiagonal
    A :: BlockDiagonalMatrix
end

function InvertedBlockDiagonal(A :: SparseMatrixCSC, bs :: Int)
    InvertedBlockDiagonal(inv(block_diag(A, bs)))
end

function LinearAlgebra.ldiv!(x :: AbstractVector, A :: InvertedBlockDiagonal, b :: AbstractVector)
    for ((i, j), block) in A.A
        x[i] = block * b[j]
    end
    x
end

function LinearAlgebra.:\(A :: InvertedBlockDiagonal, b :: AbstractVector)
    x = similar(b)
    ldiv!(x, A, b)
    x
end

function LinearAlgebra.factorize(A :: BlockDiagonalMatrix)
    InvertedBlockDiagonal(inv(A))
end
