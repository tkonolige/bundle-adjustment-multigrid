module bamg

using SparseArrays
using LinearAlgebra
using IterativeSolvers
using Preconditioners
using Formatting
using BALUtils
using DataFrames
using Rotations
using PlyIO
using Statistics
using ConcaveHull
using PrettyTables
using Humanize
using PyCall
using HDF5
using SuiteSparse
using Arpack
using ArgParse
using Profile
using LinearMaps

export create_multigrid, Multigrid, nullspace, work, info, Options, csv, bamg_solver

include("leastsquares.jl")

function affinity(A, bs; nvec = 5, numiter=3)
    b = zeros(size(A, 2), 1)
    x = rand(size(A, 1), nvec)
    pbjacobi! = make_pbjacobi(A, bs)
    pbjacobi!(x, A, b, maxiter=numiter)

    # construct structure of interblock connections
    bnnz = nnz(A)
    n = div(bnnz, bs*bs)
    is = []
    sizehint!(is, n)
    js = []
    sizehint!(js, n)
    ks = []
    sizehint!(ks, n)

    dim = div(size(A, 1), bs)

    for i in eachindex(A)
        # only iterate over the start of each off-diagonal block
        if (i[1]-1) % bs == 0 && (i[2]-1) % bs == 0 && div(i[1]-1, bs) != div(i[2]-1, bs)
            ix = div(i[1]-1, bs)+1
            jx = div(i[2]-1, bs)+1

            block_diff = x[i[1]:(i[1]+bs-1), :] - x[i[2]:(i[2]+bs-1), :]
            v = map(1:bs) do j
                norm(block_diff[j, :], Inf)
            end |> x->norm(x, Inf)

            push!(is, ix)
            push!(js, jx)
            push!(ks, 1/v)
        end
    end

    sparse(is, js, ks, dim, dim)
end

function visibility_strength(ba :: BA)
    A = convert(SparseMatrixCSC{Float64}, visibility_graph(ba))
    rows = rowvals(A)
    vals = nonzeros(A)
    for j in 1:size(A,2)
        for i in nzrange(A, j)
            row = rows[i]
            vals[i] /= length(ba.observations[row]) * length(ba.observations[j])
        end
    end
    A
end

"""
Strength of connection metric using the total shared area in the camera plane
of points seen by two cameras.
"""
function visibility_coverage(ba :: BA)
    # cameras that view each point
    point_to_cameras = Dict{Int, Set{Int}}()
    for (cam, obs) in enumerate(ba.observations)
        for (o, _, _) in obs
            push!(get!(() -> Set{Int}(), point_to_cameras, o), cam)
        end
    end

    is = []
    js = []
    ks = []
    for i in 1:num_cameras(ba)
        camera_to_points = Dict{Int, Vector{Tuple{Int, Tuple{Float64, Float64}}}}()
        for (p, x, y) in ba.observations[i]
            for c in point_to_cameras[p]
                push!(get!(() -> Vector{Tuple{Int, Tuple{Float64, Float64}}}(), camera_to_points, c), (p, (x, y)))
            end
        end

        for (camera, points) in camera_to_points
            # xys = extrema(hcat(map(x -> [x[2]...], points)...), dims=2)
            # (xmin, xmax) = xys[1]
            # (ymin, ymax) = xys[2]
            # area = (xmax - xmin) * (ymax - ymin)

            # stds = std(hcat(map(x -> [x[2]...], points)...), dims=2)
            # area = stds[1] * stds[2]

            a = if length(points) > 2
                    area(concave_hull(map(x -> [x[2]...], points)))
                else
                    xys = extrema(hcat(map(x -> [x[2]...], points)...), dims=2)
                    (xmin, xmax) = xys[1]
                    (ymin, ymax) = xys[2]
                    (xmax - xmin) * (ymax - ymin)
                end

            push!(is, i)
            push!(js, camera)
            push!(ks, a)
        end
    end

    sparse(is, js, ks, num_cameras(ba), num_cameras(ba))
end

function form_aggregates(S)
    # aggregates[i] == -1 if unaggregated, otherwise id of aggregate root
    aggregates = fill(-1, size(S, 1))
    agg_sizes = zeros(Int, size(aggregates))

    # sort by strength of connection, largest first
    # TODO: this should be symmetric
    inds = sort(collect(zip(findnz(S)...)), by=x->x[3], rev=true)
    # iterate over all edges
    for (i, j, k) in inds
        if k < 10
            break
        end
        # ignore diagonal entries
        if i == j
            continue
        end
        if aggregates[i] != -1 && aggregates[j] != -1
            # both ends of the edge are already aggregated
            continue
        end
        if aggregates[i] != -1 || aggregates[j] != -1
            # one end is aggregated, connect to that
            agg = max(aggregates[i], aggregates[j])
            # limit agg size
            if agg_sizes[agg] < 8
                aggregates[i] = agg
                aggregates[j] = agg
                agg_sizes[agg] += 1
            end
        else
            # neither end is aggregated, form new aggregate
            agg = max(i, j)
            aggregates[i] = agg
            aggregates[j] = agg
            agg_sizes[agg] = 2
        end
    end

    # handle unaggregated vertices
    off = maximum(aggregates)
    unaggregated = aggregates .== -1
    if count(unaggregated) > 0
        println("$(count(unaggregated)) unaggregated vertices out of $(length(unaggregated))")
    end
    aggregates[unaggregated] = (off+1):(off+count(unaggregated))

    aggregates
end

"""
Form aggregates from a strength of connection matrix S.
Returns an array x of size(S) where x_i = j indicates vertex i is part of aggregate j.

Aggregates do not have a limited diameter.
Aggregates are formed by iterating over vertices, then edges of S is order of weight and
aggregating the two ends of the edge together.
Aggregates are limited in size to 20.
"""
function form_aggregates_vertex_centric(S, opts)
    # aggregates[i] == -1 if unaggregated, otherwise id of aggregate root
    aggregates = fill(-1, size(S, 1))
    agg_sizes = zeros(Int, size(aggregates))

    rows = rowvals(S)
    vals = nonzeros(S)
    for i in 1:length(aggregates)
        # vertex is already aggregated; go to next vertex
        if aggregates[i] != -1
            continue
        end

        # consider edges from this vertex in order of weight
        r = nzrange(S, i)
        for (j, v) in sort(collect(zip(r, vals[r])), by=x->x[2], rev=true)
            # TODO: is filtering relevant?
            # TODO: should we normalize these on the coarse levels?
            # if v < 0.2
            #     break
            # end

            neighbor = rows[j]
            if neighbor == i
                continue
            end
            agg = aggregates[neighbor]
            if agg != -1
                if agg_sizes[agg] < opts.max_agg_size
                    aggregates[i] = agg
                    agg_sizes[agg] += 1
                    break
                end
            else
                agg = max(neighbor, i)
                aggregates[neighbor] = agg
                aggregates[i] = agg
                agg_sizes[agg] = 2
                break
            end
        end
    end

    # aggregate vertices that were not aggregated
    for i in 1:length(aggregates)
        # vertex is already aggregated
        if aggregates[i] != -1
            continue
        end
        r = nzrange(S, i)
        for (j, v) in sort(collect(zip(r, vals[r])), by=x->x[2], rev=true)
            neighbor = rows[j]
            if neighbor == i
                continue
            end
            agg = aggregates[neighbor]
            if agg != -1
                aggregates[i] = agg
                agg_sizes[agg] += 1
                break
            end
        end
    end

    aggregates
end

function form_aggregates_verify(S, opts)
    # aggregates[i] == -1 if unaggregated, otherwise id of aggregate root
    aggregates = fill(-1, size(S, 1))
    agg_sizes = zeros(Int, size(aggregates))

    rows = rowvals(S)
    vals = nonzeros(S)
    for t in 1:3
        thresh = opts.closeness_threshold * 0.6^(t-1)
        for i in 1:length(aggregates)
            # vertex is already aggregated; go to next vertex
            if aggregates[i] != -1
                continue
            end

            # consider edges from this vertex in order of weight
            r = nzrange(S, i)
            # @show extrema(vals[r])
            # @show median(vals[r])
            # @show mean(vals[r])
            for (j, v) in sort(collect(zip(r, vals[r])), by=x->x[2], rev=true)
                # TODO: is filtering relevant?
                # TODO: should we normalize these on the coarse levels?
                if v < thresh
                    break
                end

                neighbor = rows[j]
                if neighbor == i
                    continue
                end
                agg = aggregates[neighbor]
                if agg != -1
                    # check if this vertex is well connected to everyone else in the aggregate
                    count_good = 0
                    count_bad = 0
                    for k in r
                        if aggregates[rows[k]] == agg
                            if vals[k] > thresh
                                count_good += 1
                            else
                                count_bad += 1
                            end
                        end
                    end
                    if count_good / (count_good + count_bad) > opts.connectedness_ratio
                        # limit aggregate sizes
                        if agg_sizes[agg] < opts.max_agg_size/t
                            aggregates[i] = agg
                            agg_sizes[agg] += 1
                            break
                        end
                    end
                else
                    agg = max(neighbor, i)
                    aggregates[neighbor] = agg
                    aggregates[i] = agg
                    agg_sizes[agg] = 2
                    break
                end
            end
        end
    end

    @show count(aggregates .== -1)
    # aggregate vertices that were not aggregated
    for i in 1:length(aggregates)
        # vertex is already aggregated
        if aggregates[i] != -1
            continue
        end
        r = nzrange(S, i)
        for (j, v) in sort(collect(zip(r, vals[r])), by=x->x[2], rev=true)
            neighbor = rows[j]
            if neighbor == i
                continue
            end
            agg = aggregates[neighbor]
            if agg != -1
                aggregates[i] = agg
                agg_sizes[agg] += 1
                break
            end
        end
    end

    aggregates
end

function form_aggregates_max_dist(S)
    # aggregates[i] == -1 if unaggregated, otherwise id of aggregate root
    aggregates = fill(-1, size(S, 1))
    agg_sizes = zeros(Int, size(aggregates))
    agg_depth = zeros(Int, size(aggregates))

    rows = rowvals(S)
    vals = nonzeros(S)
    for i in 1:length(aggregates)
        # vertex is already aggregated; go to next vertex
        if aggregates[i] != -1
            continue
        end

        # consider edges from this vertex in order of weight
        r = nzrange(S, i)
        for (j, v) in sort(collect(zip(r, vals[r])), by=x->x[2], rev=true)
            # TODO: is filtering relevant?
            # TODO: should we normalize these on the coarse levels?
            if v < 0.2
                break
            end

            neighbor = rows[j]
            if neighbor == i
                continue
            end
            agg = aggregates[neighbor]
            if agg != -1
                if agg_sizes[agg] < 20 && agg_depth[neighbor] <= 1
                    aggregates[i] = agg
                    agg_sizes[agg] += 1
                    agg_depth[i] = agg_depth[neighbor]+1
                    break
                end
            else
                agg = max(neighbor, i)
                aggregates[neighbor] = agg
                aggregates[i] = agg
                agg_sizes[agg] = 2
                agg_depth[min(neighbor, i)] = 1
                agg_depth[agg] = 0
                break
            end
        end
    end

    # aggregate vertices that were not aggregated
    for i in 1:length(aggregates)
        # vertex is already aggregated
        if aggregates[i] != -1
            continue
        end
        r = nzrange(S, i)
        for (j, v) in sort(collect(zip(r, vals[r])), by=x->x[2], rev=true)
            neighbor = rows[j]
            if neighbor == i
                continue
            end
            agg = aggregates[neighbor]
            if agg != -1
                aggregates[i] = agg
                agg_sizes[agg] += 1
                break
            end
        end
    end

    aggregates
end

function form_aggregates_nodal(S)
    aggregates = fill(-1, size(S, 1))
    rows = rowvals(S)
    vals = nonzeros(S)

    # Determine root nodes
    # In order of total weight in the strength of connection graph, mark each
    # vertex as a root node if it is not adjacent to an existing root node
    weights = sum(S, dims=1)[:]
    for (i, _) in sort(collect(enumerate(weights)), by=x->x[2] ,rev=true)
        if aggregates[i] != -1
            continue
        end

        # check if any neighbors are root nodes
        r = nzrange(S, i)
        if !any(aggregates[rows[r]] .!= -1)
            aggregates[i] = i
        end
    end

    for (i, _) in sort(collect(enumerate(weights)), by=x->x[2] ,rev=true)
        if aggregates[i] != -1
            continue
        end

        r = nzrange(S, i)
        for (j, v) in sort(collect(zip(r, vals[r])), by=x->x[2], rev=true)
            if v < 3
                break
            end

            neighbor = rows[j]
            if neighbor == i
                continue
            end
            agg = aggregates[neighbor]
            if agg != -1
                aggregates[i] = agg
                break
            end
        end
    end

    # unaggregate aggregates of one node
    agg_sizes = fill(0, size(aggregates))
    for agg in aggregates
        agg_sizes[agg] += 1
    end
    for (i, sz) in enumerate(agg_sizes)
        if sz == 1
            aggregates[i] = -1
        end
    end

    aggregates
end

"""
Renumber aggregates to that they are contiguous.
"""
function renumber_aggregates(aggs :: Vector)
    aggids = unique(aggs)
    off = 1
    for id in aggids
        if id == -1
            continue
        end

        aggs[aggs .== id] .= off
        off += 1
    end

    aggs, off-1
end

"""
Remove vertices from aggregates based on conditions in the paper "An Efficient
Multigrid Method for Graph Laplacians II"
"""
function improve_aggregates(A, S, poses, dscale, aggregates, use_constant_vecs, bs, tol=0.1, safety=2)
    aggs = unique(aggregates)

    mews = []
    mu2 = []
    num_split = 0
    for agg in aggs
        if agg == -1
            continue
        end

        # construct prolongation on this aggregate
        inds = aggregates .== agg
        Q, _ = single_agg_low_energy_prolongation(poses[:, inds], dscale[:, inds], use_constant_vecs, bs)
        # convert indices to blocks in A
        blkinds = mapreduce(i -> ((i-1)*bs+1):i*bs, vcat, findall(inds))
        M = block_diag(A[blkinds, blkinds])
        T = M*(I - Q * inv(Q' * M * Q) * Q' * M)

        eg = eigen(T, Matrix(A[blkinds, blkinds]))

        # small eigenvalues corresponding to modes not well handled by the aggregate
        small_evals = findall(abs.(eg.values) .< 1e-10)
        # orthogonalize small eigenvectors against nullspace
        V, R = qr(hcat(Q[:,1:16], real.(eg.vectors)[:, small_evals]))

        # display(Matrix(S[inds, inds]))
        # println("")
        #
        # # show small eigenvalues that are not in the nullspace
        # is = findall(abs.(diag(R)[17:end]) .> 1e-10)
        # println("$(length(is)) non-nullspace 0 eigenvalues")
        # for i in is
        #     visualize(poses[:, inds], reshape(V[:,16+i], 9, :).*dscale[:,inds]) |> display
        #     readline(stdin)
        # end

        evals = filter(collect(enumerate(eg.values))) do (i, v)
            _, R = qr(hcat(Q, eg.vectors[:, i]))
            abs.(R[end,end]) > 1e-10
        end |> x -> getindex.(x, 2)
        push!(mews, minimum(real.(evals)))

        block = I - Q * inv(Q' * Q) * Q'
        U, V, _ = svd(block)
        l = findlast(x -> x > 1e-14, V)
        B = U[:, 1:l]
        e = eigen(B' * inv(M) * B * B' * A[blkinds, blkinds] * B)
        evs = real.(e.values)
        evcs = real.(e.vectors)
        push!(mu2, minimum(real.(evs)))
        # visualize(poses[:, inds], reshape(B * evs[:, end], 9, :) .* dscale[:, inds]) |> display
        # readline(stdin)
        # display(reshape(B * evs[:, end], 9, :))
        # println("")
        # display(mapslices(x -> dot(x, U[:,1]), reshape(B * evs[:, end], 9, :), dims=1))
        # println("")
        # println("======================")
        mv, mi = findmin(evs)
        if mv < 0.1
            @show mv
            num_split += 1
            # use sign of principle component to split aggregate
            D = reshape(B * evcs[:, mi], 9, :)
            U,_,_ = svd(D)
            split = mapslices(x -> dot(x, U[:,1]), D, dims=1)[:]
            sm = split .< 0
            lg = split .>= 0
            is = findall(inds)
            # S[inds,inds][sm, sm]
            # S[inds,inds][lg, lg]
            if length(is[lg]) <= 1
                aggregates[is[lg]] .= -1
            else
                aggregates[is[lg]] .= maximum(is[lg])
            end
            if length(is[sm]) <= 1
                aggregates[is[sm]] .= -1
            else
                aggregates[is[sm]] .= maximum(is[sm])
            end
        end
    end
    println("Split $num_split aggregates")

    # display(sort(mews))
    # println("")
    # println("=============== Mu ==============")
    # display(sort(mu2))
    # println("")

    # throw("error")

    aggregates
end

function piecewise_constant_prolongation(aggregates; bs = 9)
    ijk = []
    sizehint!(ijk, length(aggregates) * bs)
    for (i, agg) in enumerate(aggregates)
        for j in 1:bs
            push!(ijk, ((i-1)*bs+j, (agg-1)*bs+j, 1.0))
        end
    end

    # 'cause there still isn't an unzip function in julia
    is = getindex.(ijk, 1)
    js = getindex.(ijk, 2)
    ks = getindex.(ijk, 3)

    P = sparse(is, js, ks, length(aggregates)*bs, maximum(aggregates)*bs)
    # remove empty columns
    P[:, sum(P, dims=1)[:] .!= 0.0]
end

function poses_to_centers(poses)
    Rs = map(i -> RodriguesVec(poses[1:3, i]...), 1:size(poses,2))
    ts = map(i -> poses[4:6, i], 1:size(poses,2))
    -(inv.(Rs) .* ts)
end

"""
Linearized rotation of the composition of R and axis.

Composition of two angle axis vectors is given here: https://math.stackexchange.com/questions/382760/composition-of-two-axis-angle-rotations.
Derivative of composition is in the `nullspace_derivatives.nb` notebook.
"""
function linearized_rotation(R :: SVector{3,Float64}, axis :: SVector{3,Float64})
    sqrd_norm = R[1]^2 + R[2]^2 + R[3]^2
    if sqrd_norm < eps(Float64)^2
        -axis
    else
        alpha = sqrt(sqrd_norm)
        r = R/alpha
        a = axis
        0.5*abs(csc(alpha/2))*(r*(-alpha*cos(alpha/2)+sqrt(2-2*cos(alpha)))*dot(r,-a)+alpha*(-a*cos(alpha/2)+cross(r,-a)*sin(alpha/2)))
    end
end

"""
Create the nullspace for a bundle adjustment problem using snavely reprojection error with 3 rotational, 3 translational, and 3 intrinsic parameters.

The nullspace is:
3 translational components (of the whole system)
3 rotational components (of the whole system)
1 scaling component (of the whole system)
9 constant vectors if `use_constant_vecs` is true (one for each parameter)

Returns a # of parameters (9) × size of nullspace array × # of cameras
"""
function nullspace(poses :: Matrix, dscale :: Matrix, use_constant_vecs :: Bool, num_constant_vecs :: Int; bs = 9)
    num_cols = if use_constant_vecs 7+num_constant_vecs else 7 end
    # starting block index for each node in the aggregate
    l = size(poses, 2)
    ns = zeros(bs, num_cols, l)

    for bi in 1:l
        R = RodriguesVec(poses[1:3, bi]...)
        T = view(ns, :, :, bi)

        # translations
        T[4:6,1] = - (R * [1, 0, 0])
        T[4:6,2] = - (R * [0, 1, 0])
        T[4:6,3] = - (R * [0, 0, 1])

        # scaling
        T[4:6,4] = poses[4:6, bi]

        # rotations
        T[1:3,5] = linearized_rotation(SVector{3,Float64}(poses[1:3, bi]), SVector{3,Float64}([1, 0, 0]))
        T[1:3,6] = linearized_rotation(SVector{3,Float64}(poses[1:3, bi]), SVector{3,Float64}([0, 1, 0]))
        T[1:3,7] = linearized_rotation(SVector{3,Float64}(poses[1:3, bi]), SVector{3,Float64}([0, 0, 1]))

        if use_constant_vecs
            T[diagind(T, 7)] .= 1
        end

        # scale entire block
        T ./= dscale[:, bi][:]
    end

    ns
end

function low_energy_prolongation(aggregates :: Vector{Int}, num_aggs :: Int, ns :: Array{T, 3}) where T
    bs = size(ns, 1)
    is = Vector{Int}()
    sizehint!(is, length(aggregates) * 18)
    js = Vector{Int}()
    sizehint!(is, length(aggregates) * 18)
    ks = Vector{Float64}()
    sizehint!(is, length(aggregates) * 18)

    num_cols = size(ns, 2)

    aggs = sort(unique(aggregates))
    K = zeros(num_cols, num_cols, length(aggs))
    @inbounds for (i, agg) in enumerate(aggs)
        # TODO: what happens in the case where we only have one node in the aggregate?
        # TODO: eliminate?
        if agg == -1
            continue
        end

        inds = findall(aggregates .== agg)
        if length(inds) < 2
            throw("Aggregate $agg ($(inds[1])) has only one node")
        end
        Q_, R = qr(reshape(permutedims(ns[:, :, inds], [1,3,2]), :, num_cols))
        K[:, :, i] = R

        Q = Matrix(Q_)
        @inbounds for i in CartesianIndices((size(Q, 1), min(size(Q, 2), num_cols)))
            @assert i[2] <= num_cols
            q = Q[i]
            if q != 0
                # block index
                bi = (i[1] - 1) ÷ bs + 1
                # index in block
                ib = (i[1] - 1) % bs + 1
                push!(is, (inds[bi]-1)*bs + ib)
                push!(js, (agg-1)*num_cols + i[2])
                push!(ks, q)
            end
        end
    end

    P = sparse(is, js, ks, length(aggregates)*bs, num_aggs*num_cols)
    # remove empty columns
    # TODO: this should be unnecessary if aggregates are contiguous
    # to_keep = vcat(map(1:num_cols:size(P,2)) do i
    #                    # check if no columns in the block column have nonzero elements
    #                    if all(x -> x <= 0, map(j -> length(nzrange(P, j)), i:i+num_cols-1))
    #                        falses(num_cols)
    #                    else
    #                        trues(num_cols)
    #                    end
    #                end...)
    # @show count(to_keep), size(P, 2)
    # P = P[:, to_keep]

    P, K
end

function jacobi_smooth_matrix(A, implicit, P, bs)
    bd = block_diag(A, bs)
    B = InvertedBlockDiagonal(inv(bd))
    lambda_, _ = eigs(implicit, BlockDiagWithInv(B, bd), nev=1, which=:LM, maxiter=5, tol=0.01)
    lambda = maximum(real.(lambda_))
    (I - 4 / (3 * lambda) * SparseMatrixCSC(B.A) * A) * P, B, lambda
end

Base.@kwdef mutable struct Options
    use_constant_vecs :: Bool = true
    num_constant_vecs :: Int = 9
    aggregation :: String = "vertex_centric"
    max_agg_size :: Int = 20
    num_levels :: Int = 10
    coarse_size :: Int = 1000
    strength :: String = "visibility"
    smooth_prolongation :: String = "none"
    improve :: Bool = false
    smoother :: String = "pbjacobi"
    presmooth :: Int = 2
    postsmooth :: Int = 2
    cycle :: String = "vcycle"
    recompute :: Int = 1
    dump :: String = ""
    closeness_threshold :: Float64 = 1.0
    normalize_strength :: Bool = false
    connectedness_ratio :: Float64 = 0.5
end

function csv(opts :: Options)
    DataFrame([map(x -> [getfield(opts, x)], fieldnames(Options))...], [fieldnames(Options)...])
end

mutable struct AggregationLevel{T}
    A :: Union{Nothing, SparseMatrixCSC{T}}
    R :: Union{Nothing, SparseMatrixCSC{T}}
    P :: Union{Nothing, SparseMatrixCSC{T}}
    S :: Union{Nothing, SparseMatrixCSC{T}}
    x :: Vector{T}
    r :: Vector{T}
    rr :: Vector{T}
    ns :: Union{Nothing, Array{T, 3}}
    aggs :: Vector{Int}
    num_aggs :: Int
    size :: Int
    smoother
    implicit
end

mutable struct CoarseLevel{T}
    A :: Union{Nothing, SparseMatrixCSC{T}}
    ns :: Union{Nothing, Array{T, 3}}
    size :: Int
    Af
end

MultigridLevel = Union{AggregationLevel, CoarseLevel}

mutable struct Multigrid
    levels :: Vector{MultigridLevel}
    opts :: Options
end

function Base.size(mg :: Multigrid)
    (mg.levels[1].size, mg.levels[1].size)
end

function Base.size(mg :: Multigrid, i :: Int)
    size(mg)[i]
end

function info(agg :: AggregationLevel)
    DataFrame(:Size => agg.size, :NNZ => agg.A == nothing ? -1 : nnz(agg.A), :Type => "Agg", :FLOPS => flops(agg), :Blocks => agg.ns == nothing ? -1 : size(agg.ns, 3), :BlockSize => agg.ns == nothing ? -1 : size(agg.ns, 1))
end

function info(cr :: CoarseLevel)
    DataFrame(:Size => cr.size, :NNZ => cr.Af == nothing ? -1 : nnz(cr.Af), :Type => "Coarse", :FLOPS => flops(cr), :Blocks => cr.ns == nothing ? -1 : size(cr.ns, 3), :BlockSize => cr.ns == nothing ? -1 : size(cr.ns, 1))
end

function info(mg :: Multigrid)
    lvl_info = vcat(info.(mg.levels)...)
end

function Base.show(io :: IO, mg :: Multigrid)
    println(io, "AMG with levels:")
    df = info(mg)
    pretty_table(io, df, markdown; nosubheader=true, formatter=ft_printf("%'d", [1, 2, 4]))
end

function Base.show(io :: IO, x :: AggregationLevel)
    print(io, "Aggregation level with $(format(nnz(x.A), commas=true)) nonzeros")
end

function Base.show(io :: IO, x :: CoarseLevel)
    print(io, "Coarse level with $(format(nnz(x.A), commas=true)) nonzeros")
end

function create_multigrid(visibility :: SparseMatrixCSC{Float64}, options :: Options)
    Multigrid(_create_multigrid(visibility, 1, options), options)
end

function create_multigrid(ba :: BA, options :: Options)
    S = if options.strength == "visibility"
            visibility_strength(ba)
        elseif options.strength == "coverage"
            visibility_coverage(ba)
        elseif options.strength == "affinity"
            affinity(A)
        else
            throw("Unknown strength of connection type $(options.strength)")
        end
    Multigrid(_create_multigrid(S, 1, options), options)
end

function _create_multigrid(S :: SparseMatrixCSC{Float64}, level :: Int, options :: Options) where T
    if options.normalize_strength
        d = 1 ./ sqrt.(sum(S, dims=1)[:])
        S = Diagonal(d) * S * Diagonal(d)
    end

    # TODO: remove hard coding
    bs = if level == 1 9 else 16 end
    if size(S, 1) * bs < options.coarse_size || level >= options.num_levels
        [CoarseLevel{Float64}(nothing, nothing, size(S,1)*bs, nothing)]
    else
        aggs = if options.aggregation == "vertex_centric"
                   form_aggregates_vertex_centric(S, options)
               elseif options.aggregation == "regular"
                   form_aggregates(S)
               elseif options.aggregation == "nodal"
                   form_aggregates_nodal(S)
               elseif options.aggregation == "max_dist"
                   form_aggregates_max_dist(S)
               elseif options.aggregation == "verify"
                   form_aggregates_verify(S, options)
               else
                   throw("Unknow aggregation type $(options.aggregation)")
               end
        if options.improve
            aggs = improve_aggregates(A, S, K, aggs)
        end
        aggs, num_aggs = renumber_aggregates(aggs)

        dump(options, 0, "agg_$level", aggs)

        # handle unaggregated vertices
        unaggregated = aggs .== -1
        if count(unaggregated) > 0
            println("$(count(unaggregated)) unaggregated vertices out of $(length(unaggregated))")
        end


        # prolongation for the strength of connection matrix
        P_ = sparse((1:length(aggs))[aggs .!= -1], aggs[aggs .!= -1], ones(length(aggs))[aggs .!= -1], length(aggs), num_aggs)
        Sc = P_' * S * P_


        sz = size(S,1) * bs
        lvl = AggregationLevel{Float64}(nothing, nothing, nothing, S, zeros(sz), zeros(sz), zeros(size(P_,2) * 16), nothing, aggs, num_aggs, sz, nothing, nothing)

        vcat(lvl, _create_multigrid(Sc, level+1, options))
    end
end

struct ImplicitMatrix
    implicit_mm!
    size :: Tuple{Int, Int}
    nnz :: Int
end

LinearMaps.LinearMap(A :: ImplicitMatrix) = LinearMaps.LinearMap((x,y) -> A.implicit_mm!(x,y), A.size[1]; issymmetric=true, ismutating=true)
LinearAlgebra.mul!(y, A :: ImplicitMatrix, x) = A.implicit_mm!(x, y)
Base.eltype(A :: ImplicitMatrix) = Float64
LinearAlgebra.checksquare(A :: ImplicitMatrix) = A.size[1]
LinearAlgebra.issymmetric(A :: ImplicitMatrix) = true
Base.size(A :: ImplicitMatrix) = A.size
Base.size(A :: ImplicitMatrix, i::Integer) = A.size[i]
SparseArrays.nnz(A :: ImplicitMatrix) = A.nnz

function update!(mg :: Multigrid, colptr :: Vector, rows :: Vector, values :: Vector, dscale :: Vector, poses :: Vector, mm_ptr :: Ptr, nnz :: Int)
    try
        # ceres gives us only the lower part of the matrix
        A = SparseMatrixCSC{Float64,Int64}(Symmetric(SparseMatrixCSC(size(mg)..., colptr .+ 1, rows .+ 1, values), :L))
        implicit = if mm_ptr == C_NULL
            nothing
        else
            @assert nnz > 0
            ImplicitMatrix(
                function implicit_mm!(x :: AbstractVector, y :: AbstractVector)
                    ccall(mm_ptr, Cvoid, (Ptr{Float64}, Ptr{Float64}), x, y)
                end,
                size(A),
                nnz
            )
        end
        update!(mg, A, reshape(dscale, 9, :), reshape(poses, 9, :), implicit)
    catch e
        println(stderr, "ERROR: $e")
        for x in stacktrace(catch_backtrace())
            println(stderr, x)
        end
        throw(e)
    end
end

matfile = nothing
iter = 0

function update!(mg :: Multigrid, A :: SparseMatrixCSC{Float64,Int64}, dscale :: Matrix{Float64}, poses :: Matrix{Float64}, implicit)
    global iter += 1
    dump(mg.opts, iter, "A", A)
    dump(mg.opts, iter, "scale", dscale)
    dump(mg.opts, iter, "poses", poses)

    GC.gc()

    ns = nullspace(poses, dscale, mg.opts.use_constant_vecs, mg.opts.num_constant_vecs)
    for i in 1:7
        dump(mg.opts, iter, "nullspace$i", ns[:,i,:])
    end

    # ns_ = nullspace(poses, dscale, false, 8)
    # Q, _ = qr(reshape(permutedims(ns_, [1,3,2]), :, size(ns_, 2)))
    # res =  mapslices(norm, A * Matrix(Q), dims=1)
    # @show res
    # @assert all(x -> x < 1e-8, res)

    _update!(mg.levels, A, ns, mg.opts, implicit, 1)
end

# TODO: only update the top level A matrix to improve setup cost
function _update!(mg :: Vector{MultigridLevel}, A :: SparseMatrixCSC{Float64}, ns :: Array{Float64, 3}, options :: Options, implicit, lvli)
    lvl = mg[1]
    if typeof(lvl) <: CoarseLevel
        lvl.A = A
        lvl.ns = ns
        lvl.Af = factorize(A)
    else
        # free next level memory
        mg[2].A = nothing

        bs = size(ns, 1)
        lvl.A = A
        if implicit == nothing
            lvl.implicit = A
        else
            lvl.implicit = implicit
        end

        lvl.P, K = low_energy_prolongation(lvl.aggs, lvl.num_aggs, ns)
        B = nothing
        lambda = nothing
        if options.smooth_prolongation == "jacobi"
            lvl.P, B, lambda_ = jacobi_smooth_matrix(A, lvl.implicit, lvl.P, bs)
            lambda = lambda_[1]
        elseif options.smooth_prolongation == "energy"
            pyamg = pyimport("pyamg")
            scipy = pyimport("scipy.sparse")
            K_ = reshape(permutedims(K, [3, 1, 2]), :, size(K, 2))
            ns_ = reshape(permutedims(K, [3, 1, 2]), :, size(ns, 2))
            P_ = scipy.bsr_matrix(P, blocksize=(size(ns,1), size(K,1)))
            A_ = scipy.bsr_matrix(A, blocksize=(size(ns,1), size(ns,1)))
            S_ = scipy.csr_matrix(S)
            Ppy = pyamg.aggregation.smooth.energy_prolongation_smoother(A_, P_, S_, K_, ns_, (false, Dict()), weighting="block", degree=1)
            # Ppy = pyamg.aggregation.smooth.jacobi_prolongation_smoother(A_, P_, S_, K_, weighting="block", filter=true)
            is, js, ks = scipy.find(Ppy)
            lvl.P = sparse(is .+ 1, js .+ 1, ks, size(P)...)
        end

        lvl.R = copy(lvl.P')
        # We use suitesparse to do the multiply here as julia seems to over allocate memory
        AA = sparse(SuiteSparse.CHOLMOD.Sparse(lvl.R) * SuiteSparse.CHOLMOD.Sparse(lvl.A, 0) * SuiteSparse.CHOLMOD.Sparse(lvl.P))

        lvl.ns = ns

        # free memory before creating
        lvl.smoother = nothing
        # TODO: use implicit representation to build preconditioner?
        lvl.smoother = build_smoother(lvl.A, lvl.implicit, bs, options.smoother, B, lambda, lvl.aggs)

        implicit_coarse = if implicit != nothing && nnz(lvl.R) + nnz(lvl.P) + nnz(lvl.implicit) < nnz(AA)
            @assert nnz(lvl.implicit) > 0
            ImplicitMatrix(
                function implicit_mm!(x :: AbstractVector, y :: AbstractVector)
                    z = lvl.P * x
                    w = similar(z)
                    lvl.implicit.implicit_mm!(z, w)
                    mul!(y, lvl.R, w)
                end,
                size(AA),
                nnz(lvl.R) + nnz(lvl.P) + nnz(lvl.implicit)
           )
        else
            nothing
        end

        dump(options, iter, "P_$lvli", lvl.P)
        dump(options, iter, "A_$lvli", lvl.A)

        _update!(mg[2:end], AA, K, options, implicit_coarse, lvli+1)
    end
end

# Separate pbjacobi into a class for faster type inference
struct PBJacobi
    lambda :: Float64
    B
end
function (pb :: PBJacobi)(x :: Vector, A, b :: Vector, iters :: Int)
    chebyshev!(x, A, b, 0.3*pb.lambda, 1.1*pb.lambda; Pl=pb.B, maxiter=iters)
end

# Lazy product of two matrices
struct MatrixProduct{T} <: AbstractMatrix{T}
  A :: AbstractMatrix{T}
  B :: AbstractMatrix{T}
  tmp :: AbstractArray
end
Base.size(A :: MatrixProduct) = (size(A.A,1), size(A.B, 2))
LinearAlgebra.mul!(y :: AbstractVector, A :: MatrixProduct, x :: AbstractVector) = mul!(y, A.A, mul!(A.tmp, A.B, x))
# LinearAlgebra.issymmetric(A :: MatrixProduct) = issymmetric(A) && issymmetric(B)
LinearAlgebra.issymmetric(A :: MatrixProduct) = true

# Gershgorin estimate of the largest eigenvalue
function gershgorin(A :: SparseMatrixCSC)
    # nzvals = nonzeros(A)
    # rows = rowvals(A)
    # map(1:size(A,2)) do i
    #     r = nzrange(A, i)
    #     nzvals[findfirst(isequal(i), rows[r])] + sum(abs.(nzvals[r[rows[r] .!= i]]))
    # end |> maximum
    maximum(sum(abs.(A - spdiagm(0 => diag(A))), dims=1)[:] .+ diag(A))
end

function gershgorin(A :: SparseMatrixCSC, B :: BlockDiagonalMatrix)
    lus = lu.(B.blocks)
    bs = size(B.blocks[1], 1)
    L = BlockDiagonalMatrix{bs,Float64,Int}(size(B)..., map(x -> inv(MMatrix{bs,bs}(x.L)), lus))
    U = BlockDiagonalMatrix{bs,Float64,Int}(size(B)..., map(x -> inv(MMatrix{bs,bs}(x.U)), lus))
    gershgorin(L * A * U)
end

# Simple struct to hold block diagonal and its inverse
# Used to avoid inverting the block diagonal twice because Arpack calls factorize
# on the right hand side matrix.
struct BlockDiagWithInv
    inverted :: InvertedBlockDiagonal
    A :: BlockDiagonalMatrix
end

LinearAlgebra.factorize(A :: BlockDiagWithInv) = A.inverted
LinearAlgebra.:\(A :: BlockDiagWithInv, x :: AbstractVector) = A.inverted \ x
LinearAlgebra.issymmetric(A :: BlockDiagWithInv) = true
Base.:*(A :: BlockDiagWithInv, x :: AbstractVector) = A.A * x

struct InvertedMatrix
    A
end
LinearAlgebra.:\(A :: InvertedMatrix, x :: AbstractVector) = A.A * x
LinearAlgebra.ldiv!(y, A :: InvertedMatrix, x) = copy!(y, A.A * x)

function make_pbjacobi(A :: SparseMatrixCSC, implicit, bs :: Int, B, lambda)
    if B == nothing && lambda == nothing
        bd = block_diag(A, bs)
        B = InvertedBlockDiagonal(inv(bd))
        # estimate largest eigenvalue
        # use lazy product for powm as it is faster
        # IMPORTANT: 10 iterations appears to be nessisary for good convergence
        # lambda1, _ = @time powm(MatrixProduct(B.A, A, zeros(Complex{Float64}, size(A,1))), maxiter=20) # eigen estimate of preconditioned operator

        # gershgorin eigenvalue estimate.
        # IMPORTANT: this is super inaccurate
        # lambda = gershgorin(B.A * A)
        # lambda_ = gershgorin(A, block_diag(A,bs))

        # Arpack lanczos eigensolve
        # lambda4, _ = @time eigs(B.A * A, nev=1, which=:LM, maxiter=5, tol=0.01)
        # Using lazy matrix product is faster and more accurate
        # TODO: can I use a generalized eigenvalue problem here
        # lambda, _ = @time eigs(MatrixProduct(B.A, A, zeros(Complex{Float64}, size(A,1))), nev=1, which=:LM, maxiter=5, tol=0.01) # have to set tol otherwise we get an error
        # TODO: the block diagonal gets inverted. is that nessisary and does it improve performance
        lambda_, _ = eigs(implicit, BlockDiagWithInv(B, bd), nev=1, which=:LM, maxiter=5, tol=0.01) # have to set tol otherwise we get an error
        lambda = lambda_[1]
        @assert real(lambda) > 0
    end
    PBJacobi(real(lambda), B)
end

struct MatWithInv
    inverted
    A
end

LinearAlgebra.factorize(A :: MatWithInv) = A.inverted
LinearAlgebra.:\(A :: MatWithInv, x :: AbstractVector) = A.inverted \ x
LinearAlgebra.issymmetric(A :: MatWithInv) = true
Base.:*(A :: MatWithInv, x :: AbstractVector) = A.A * x

function make_bjacobi(A :: SparseMatrixCSC, implicit, bs :: Int, aggs)
    num_nonzeros = mapreduce(i -> (count(x -> x == i, aggs)*bs)^2, +, unique(aggs))
    is = Vector{Int}()
    sizehint!(is, num_nonzeros)
    js = Vector{Int}()
    sizehint!(js, num_nonzeros)
    ks = Vector{Float64}()
    sizehint!(ks, num_nonzeros)
    for i in unique(aggs)
        inds = findall(aggs .== i)
        @assert length(inds) < 30
        binds = mapreduce(x -> ((x-1)*bs+1):x*bs, vcat, inds)
        binv = inv(Matrix(A[binds, binds]))
        for j in 1:length(binds)
            for k in 1:length(binds)
                push!(is, binds[j])
                push!(js, binds[k])
                push!(ks, binv[j,k])
            end
        end
    end
    Binv = sparse(is, js, ks, size(A)...)
    bis = Vector{Int}()
    sizehint!(bis, num_nonzeros)
    bjs = Vector{Int}()
    sizehint!(bjs, num_nonzeros)
    bks = Vector{Float64}()
    sizehint!(bks, num_nonzeros)
    for i in unique(aggs)
        inds = findall(aggs .== i)
        @assert length(inds) < 30
        binds = mapreduce(x -> ((x-1)*bs+1):x*bs, vcat, inds)
        binv = Matrix(A[binds, binds])
        for j in 1:length(binds)
            for k in 1:length(binds)
                push!(bis, binds[j])
                push!(bjs, binds[k])
                push!(bks, binv[j,k])
            end
        end
    end
    B = sparse(bis, bjs, bks, size(A)...)

    y = zeros(size(A,1))
    T = LinearMap(function(x)
                     mul!(y, implicit, x)
                     Binv * y
                 end, size(A,1))
    x = rand(size(A,1))
    lambda_, _ = powm!(T, x, maxiter=20, tol=1e-2) # eigen estimate of preconditioned operator

    # decomp, _ = partialschur(T; nev=1, which=ArnoldiMethod.LM(), tol=1e-3, restarts=5)
    # egs = eigenvalues(decomp.R)
    # @show egs

    # lambda_, _ = eigs(implicit, MatWithInv(B, Binv), nev=1, which=:LR, maxiter=5, tol=0.01) # have to set tol otherwise we get an error
    lambda = lambda_[1]
    if real(lambda) < 0
        lambda = lambda * -1
    end
    @assert real(lambda) > 0 "lambda, $(real(lambda)), <= 0"
    B = nothing
    PBJacobi(real(lambda), InvertedMatrix(Binv))
end

function make_block_gauss_seidel(A, bs)
    L = tril(A, 0)
    U = triu(A, 1)
    function block_gauss_seidel!(x, A, b; kwargs)
        for i in kwargs[:maxiter]
            x = L \ (b - U*x)
        end
    end
end

function richardson(A, b, x, f)
    for i in 1:10
        x = x + f(b - A*x)
    end

    x
end

function build_smoother(A, implicit, bs, smoother_name, B, lambda, aggs)
    if smoother_name == "pbjacobi"
        make_pbjacobi(A, implicit, bs, B, lambda)
    elseif smoother_name == "bjacobi"
        make_bjacobi(A, implicit, bs, aggs)
    elseif smoother_name == "gs"
        (x, A, b, iter) -> gauss_seidel!(x, A, b, maxiter=iter)
    elseif smoother_name == "sor"
        (x, A, b, iter) -> sor!(x, A, b, 0.5, maxiter=iter)
    else
        @error "Unknown smoother $smoother_name"
        exit(1)
    end
end

function vcycle(mg :: Multigrid, b :: Vector) :: Vector
    vcycle(mg.levels, b, mg.opts)
end

function vcycle(mg :: Vector{MultigridLevel}, b :: Vector, opts :: Options, level=1) :: Vector
    lvl = mg[1]
    if typeof(lvl) <: CoarseLevel
        lvl.Af \ b
        # x = cg(lvl.A, b, Pl=InvertedBlockDiagonal(lvl.A, 16), tol=1e-4)
        # x
        # lvl.A \ b
    elseif typeof(lvl) <: AggregationLevel
        lvl.x .= 0
        # println("$level Start\t\t$(norm(lvl.A*lvl.x-b))")
        lvl.x = lvl.smoother(lvl.x, lvl.implicit, b, opts.presmooth)
        # println("$level Presmooth\t$(norm(lvl.A*lvl.x-b))")

        # r = b - A * x
        mul!(lvl.r, lvl.implicit, lvl.x)
        lvl.r .*= -1
        lvl.r .+= b
        lvl.rr = lvl.R * lvl.r
        xr = vcycle(mg[2:end], lvl.rr, opts, level+1)
        lvl.x .+= lvl.P * xr

        # println("$level PostInterp\t$(norm(lvl.A*lvl.x-b))")
        lvl.x = lvl.smoother(lvl.x, lvl.implicit, b, opts.postsmooth)
        # println("$level PostSmooth\t$(norm(lvl.A*lvl.x-b))")

        lvl.x
    else
        @error "Unexpected level type $(typeof(lvl))"
    end
end

function wcycle(mg :: Multigrid, b :: Vector) :: Vector
    wcycle(mg.levels, b, mg.opts)
end

function wcycle(mg :: Vector{MultigridLevel}, b :: Vector, opts :: Options, level=1) :: Vector
    lvl = mg[1]
    if typeof(lvl) <: CoarseLevel
        lvl.Af \ b
        # x = cg(lvl.A, b, Pl=InvertedBlockDiagonal(lvl.A, 16), tol=1e-4)
        # lvl.A \ b
    elseif typeof(lvl) <: AggregationLevel
        # TODO: store x between invocations
        x = zeros(size(b))
        # println("$level Start\t\t$(norm(lvl.A*x-b))")
        x = lvl.smoother(x, lvl.A, b, opts.presmooth)
        # println("$level PostSmooth\t$(norm(lvl.A*x-b))")

        for i in 1:2
            r = b - lvl.A*x
            rr = lvl.R * r
            xr = wcycle(mg[2:end], rr, opts, 2)
            x += lvl.P * xr
            # println("$level PostInterp\t$(norm(lvl.A*x-b))")

            x = lvl.smoother(x, lvl.A, b, opts.presmooth)
            # println("$level PostSmooth\t$(norm(lvl.A*x-b))")
        end

        x
    else
        @error "Unexpected level type $(typeof(lvl))"
    end
end

function fcycle(mg :: Multigrid, b :: Vector) :: Vector
    fcycle(mg.levels, b, mg.opts)
end

function fcycle(mg :: Vector{MultigridLevel}, b :: Vector, opts :: Options) :: Vector
    lvl = mg[1]
    if typeof(lvl) <: CoarseLevel
        lvl.Af \ b
        # x = cg(lvl.A, b, tol=1e-12)
        # lvl.A \ b
    elseif typeof(lvl) <: AggregationLevel
        # TODO: store x between invocations
        x = zeros(size(b))
        x = lvl.smoother(x, lvl.A, b, opts.presmooth)
        r = b - lvl.A*x
        rr = lvl.R * r
        x += lvl.P * fcycle(mg[2:end], rr, opts)

        # println("Start\t\t$(norm(lvl.A*x-b))")
        x = lvl.smoother(x, lvl.A, b, opts.presmooth)
        # println("Presmooth\t$(norm(lvl.A*x-b))")

        r = b - lvl.A*x
        rr = lvl.R * r
        xr = vcycle(mg[2:end], rr, opts)
        x += lvl.P * xr

        # println("PostInterp\t$(norm(lvl.A*x-b))")
        x = lvl.smoother(x, lvl.A, b, opts.postsmooth)
        # println("PostSmooth\t$(norm(lvl.A*x-b))")

        x
    else
        @error "Unexpected level type $(typeof(lvl))"
    end
end

function kcycle(mg :: Multigrid, b :: Vector) :: Vector
    kcycle(mg.levels, b, mg.opts)
end

struct LDivWrapper
    func
end

LinearAlgebra.ldiv!(y, wrapper :: LDivWrapper, x) = copy!(y, wrapper.func(x))

function kcycle(mg :: Vector{MultigridLevel}, b :: Vector, opts :: Options, level=1) :: Vector
    lvl = mg[1]
    if typeof(lvl) <: CoarseLevel
        lvl.Af \ b
        # x = cg(lvl.A, b, Pl=InvertedBlockDiagonal(lvl.A, 16), tol=1e-4)
        # x
        # lvl.A \ b
    elseif typeof(lvl) <: AggregationLevel
        lvl.x .= 0
        # println("$level Start\t\t$(norm(lvl.A*lvl.x-b))")
        lvl.x = lvl.smoother(lvl.x, lvl.implicit, b, opts.presmooth)
        # println("$level Presmooth\t$(norm(lvl.A*lvl.x-b))")

        # r = b - A * x
        mul!(lvl.r, lvl.implicit, lvl.x)
        lvl.r .*= -1
        lvl.r .+= b
        lvl.rr = lvl.R * lvl.r
        xr = if typeof(mg[2]) <: CoarseLevel
            kcycle(mg[2:end], lvl.rr, opts, level+1)
        else
            cg(mg[2].implicit, lvl.rr, Pl=LDivWrapper(x->kcycle(mg[2:end], x, opts, level+1)), maxiter=2)
        end
        lvl.x .+= lvl.P * xr

        # println("$level PostInterp\t$(norm(lvl.A*lvl.x-b))")
        lvl.x = lvl.smoother(lvl.x, lvl.implicit, b, opts.postsmooth)
        # println("$level PostSmooth\t$(norm(lvl.A*lvl.x-b))")

        lvl.x
    else
        @error "Unexpected level type $(typeof(lvl))"
    end
end

# TODO: kinda weird that this is dependent on solve parameters (number of smoothing cycles)
# TODO: wrong if using fcycles
flops(ml :: CoarseLevel) = ml.Af == nothing ? -1 : nnz(ml.Af) * 2 # need two passes to solve
flops(ml :: AggregationLevel) = ml.A == nothing ? -1 : nnz(ml.A) * 5 + nnz(ml.P) + nnz(ml.R)
flops(mg :: Multigrid) = sum(map(flops, mg.levels))
work(mg :: Multigrid) = flops(mg) / nnz(mg.levels[1].A)

function Base.:\(mg :: Multigrid, b)
    dump(mg.opts, iter, "b", b)

    size(mg,2) == length(b) || DimensionMismatch()
    if mg.opts.cycle == "vcycle"
        vcycle(mg, b)
    elseif mg.opts.cycle == "wcycle"
        wcycle(mg, b)
    elseif mg.opts.cycle == "fcycle"
        fcycle(mg, b)
    elseif mg.opts.cycle == "kcycle"
        kcycle(mg, b)
    else
        @error "Unknown cycle type $(mg.opts.cycle)"
    end
end
# Needed for iterative solvers
function LinearAlgebra.ldiv!(x, mg :: Multigrid, b)
    try
        copyto!(x, mg \ b)
    catch e
        println(e)
        display(stacktrace(catch_backtrace()))
        throw(e)
    end
end

function bamg_solver(ba, opts)
    count = 0
    mg = create_multigrid(ba, opts)
    function prec(A, b, lm :: LM)
        if count % opts.recompute == 0
            count += 1
            poses = reshape(lm.solution[1:num_cameras(ba)*9], 9, :)
            dscale = reshape(lm.scale[1:num_cameras(ba)*9], 9, :)
            time = @timed update!(mg, A, dscale, poses, nothing)
            mg, Dict("setup_time" => time, "levels" => length(mg.levels))
        else
            count += 1
            mg, Dict("setup_time" => 0.0, "levels" => length(mg.levels))
        end
    end
end

function create_multigrid_ceres(colptr :: Vector{Int32}, rows :: Vector{Int32}, values :: Vector{Float64}, options_file :: String, dumpfile :: String)
    try
        opts = if options_file != ""
            s = ArgParseSettings(autofix_names = true)
            for (ty, name) in zip(fieldtypes(Options), fieldnames(Options))
                add_arg_table!(s, "--$name", Dict(:arg_type=>ty))
            end
            args = parse_args(filter(x -> x != "", split(read(options_file, String), "\n")), s)
            x = filter(x->x[1] in fieldnames(Options) && x[2] != nothing, map(x -> (Symbol(x[1]), x[2]), collect(args)))
            Options(;x...)
        else
            Options()
        end
        opts.dump = dumpfile
        if isfile(dumpfile)
            rm(dumpfile)
        end

        colptr = convert(Vector{Int64}, colptr)
        rows = convert(Vector{Int64}, rows)
        values = copy(values)

        # sort rows by indices
        for i in 1:length(colptr)-1
            if !issorted(rows[colptr[i]:colptr[i+1]-1])
                p = sortperm(rows[colptr[i]:colptr[i+1]-1])
                rows[colptr[i]:colptr[i+1]-1] = rows[colptr[i]:colptr[i+1]-1][p]
                values[colptr[i]:colptr[i+1]-1] = values[colptr[i]:colptr[i+1]-1][p]
            end
        end

        visibility = SparseMatrixCSC(length(colptr)-1, length(colptr)-1, colptr, rows, values)

        dump(opts, 0, "visibility", visibility)

        mg = create_multigrid(visibility, opts)
        mg
    catch e
        println(e)
        display(stacktrace(catch_backtrace()))
        throw(e)
    end
end

function dump(opts :: Options, iter, name, variable)
    if opts.dump != ""
        f = h5open(opts.dump, "cw")
        if !exists(f, "$iter/$name")
            if typeof(variable) <: SparseMatrixCSC
                write(f, "$iter/$name/size", collect(size(variable)))
                write(f, "$iter/$name/jc", UInt64.(variable.colptr .- 1))
                write(f, "$iter/$name/ir", UInt64.(variable.rowval .- 1))
                write(f, "$iter/$name/data", variable.nzval)
                attrs(f["$iter/$name"])["MATLAB_sparse"] = UInt64(size(variable,1))
            else
                write(f, "$iter/$name", variable)
            end
        end
        close(f)
    end
end

# Precompilation for faster type inference
include("precompile.jl")
_precompile_()

end
