"""
Block CSC Matrix.

Abuses julia's SparseMatrixCSC for most operations.
"""

struct SparseMatrixBSC{I,J,T,Ti,L} <: AbstractSparseMatrix{T,Ti}
    mat :: SparseMatrixCSC{SMatrix{I,J,T,L},Ti}
end

function SparseMatrixBSC(is :: Vector, js :: Vector, bx :: Int, by :: Int, x :: Int, y :: Int, T = Float64 :: Type)
    SparseMatrixBSC{bx,by,T,Int,bx*by}(sparse(is, js, fill(zero(SMatrix{bx,by,T}), length(is)), x÷bx, y÷by))
end

block(A :: SparseMatrixBSC, i, j) = A.mat[i,j]
function blocktype(A :: SparseMatrixBSC{I,J,T,Ti}) where {I,J,T,Ti}
    SMatrix{I,J,T}
end
function Base.eltype(A :: SparseMatrixBSC{I,J,T,Ti}) where {I,J,T,Ti}
    T
end

function bs(A :: SparseMatrixBSC{I,J}) where {I,J}
    (I,J)
end
bs(A :: SparseMatrixBSC, i) = bs(A)[i]

Base.size(A :: SparseMatrixBSC) = (size(A.mat,1)*bs(A,1), size(A.mat,2)*bs(A,2))
Base.copy(A :: SparseMatrixBSC) = SparseMatrixBSC(copy(A.mat))

SparseArrays.nonzeros(A :: SparseMatrixBSC) = reinterpret(eltype(A), nonzeros(A.mat))
SparseArrays.nnz(A :: SparseMatrixBSC) = reduce(*, [bs(A)..., nnz(A.mat)])

function Base.:+(A :: SparseMatrixBSC{I,I,T}, B :: BlockDiagonalMatrix{I,T}) where {I,T}
    broadcast(+, A, B)
end

# TODO: use broadcasting interface
# TODO: avoid copy?
function Base.broadcast(op, A :: SparseMatrixBSC{I,I,T}, B :: BlockDiagonalMatrix{I,T}) where {I,T}
    size(A) == size(B) || DimensionMismatch
    AA = copy(A)
    @inbounds for i in 1:size(AA.mat,1)
        AA.mat[i,i] = op(AA.mat[i,i], B.blocks[i])
    end
    AA
end

function blockinds(A :: SparseMatrixBSC, i, j)
    (i-1)*bs(A,1)+1:i*bs(A,1), (j-1)*bs(A,2)+1:j*bs(A,2)
end

function Base.:+(A :: SparseMatrixBSC{I,I,T}, B :: Diagonal) where {I,T}
    broadcast(+, A, B)
end

function Base.broadcast(op, A :: SparseMatrixBSC{I,I,T}, B :: Diagonal) where {I,T}
    size(A) == size(B) || DimensionMismatch
    AA = copy(A)
    @inbounds for i in 1:size(AA.mat,1)
        AA.mat[i,i] = op(AA.mat[i,i], Diagonal(B.diag[blockinds(AA, i, i)[1]]))
    end
    AA
end

function SparseArrays.SparseMatrixCSC(A :: SparseMatrixBSC{I,J,T,Ti}) where {I,J,T,Ti}
    colptr = Vector{Ti}(undef, size(A,2)+1)
    rows = Vector{Ti}(undef, nnz(A))
    nzs = Vector{T}(undef, nnz(A))

    @inbounds for (i, c) in enumerate(A.mat.colptr[1:end-1])
        for j in 1:J
            colptr[(i-1)*J+j] = (c-1)*I*J + (j-1)*length(nzrange(A.mat, i))*I + 1
        end
    end
    colptr[end] = nnz(A) + 1

    @inbounds for col in 1:size(A.mat,2)
        rng = nzrange(A.mat, col)
        for r in rng
            row = rowvals(A.mat)[r]
            off = r - rng[1]
            for k in 1:J
                for j in 1:I
                    rows[colptr[(col-1)*J+k]-1 + off*I + j] = (row-1)*I + j
                    nzs[colptr[(col-1)*J+k]-1 + off*I + j] = nonzeros(A.mat)[r][j,k]
                end
            end
        end
    end
    SparseMatrixCSC{T,Ti}(size(A)..., colptr, rows, nzs)
end

function zero!(A :: SparseMatrixBSC)
    nzs = nonzeros(A.mat)
    for i in 1:length(nzs)
        nzs[i] = zero(blocktype(A))
    end
end
