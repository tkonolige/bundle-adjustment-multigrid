using BALUtils
using DataFrames
using bamg
using LinearAlgebra
using SparseArrays
using ArgParse
using PrettyTables
import CSV
using Profile

function solver(s, ba :: BA, ctest :: String, tol :: Float64, opts :: Options)
    if s == "multigrid"
        schur_cg_solver(ba, bamg_solver(ba, opts); convergence_test=ctest, tol=tol)
    elseif s == "pbjacobi"
        schur_cg_solver(ba, pbjacobi; convergence_test=ctest, tol=tol)
    elseif s == "schur_cholesky"
        schur_solver(ba, cholesky_solver)
    elseif s == "cholesky"
        cholesky_solver
    elseif s == "lu"
        lu_solver
    elseif s == "unreduced_pbjacobi"
        c = if ctest == "nash"
            bamg.nash_converged(tol)
        elseif ctest == "rtol"
            bamg.default_converged(tol)
        else
            error("Unknown ctest $ctest")
        end
        bamg.cg_solver(converged=c)
    end
end

function log_to_df(log)
    # skip the first log, as it is before the solve
    map(enumerate(log[2:end])) do (i,l)
        df = DataFrame(problem=i)
        for k in keys(l)
            if k == "solve_info"
                for (kk, v) in l[k]
                    if !(typeof(v) <: AbstractArray)
                        df[!, Symbol(kk)] = [v]
                    end
                end
            else
                df[!, Symbol(k)] = [l[k]]
            end
        end
        df
    end |> x -> vcat(x...)
end

s = ArgParseSettings(autofix_names = true)
@add_arg_table s begin
    "input"
        help = "Bundle adjustment problem"
        required = true
    "--csv"
        help = "Output filename"
    "--solvers"
        help = "Solvers to benchmark"
        arg_type = String
        default = "pbjacobi,multigrid"
    "--convergence"
        help = "Convergence criteria"
        arg_type = String
        default = "nash"
    "--tol"
        help = "Convergence tolerance"
        arg_type = Float64
        default = 0.05
    "--profile"
        action = :store_true
    "--maxiter"
        arg_type = Int
        default = 100
    "--nash-log"
        arg_type = String
        default = ""
    "--out"
        arg_type = String
        default = ""
end
for (ty, name) in zip(fieldtypes(Options), fieldnames(Options))
    add_arg_table(s, "--$name", Dict(:arg_type=>ty))
end
args = parse_args(ARGS, s)
x = filter(x->x[1] in fieldnames(Options) && x[2] != nothing, map(x -> (Symbol(x[1]), x[2]), collect(args)))
opts = Options(;x...)

println("Reading files...")
ba = readbal(args["input"])
println(ba)

println("Running solvers...")
df = map(split(args["solvers"],",")) do s
    solve = solver(s, ba, args["convergence"], args["tol"], opts)
    # warmup
    println("Warming up $s solver")
    @time bundleadjust(ba, maxiter=1, solve=solve, verbose=false)
    Profile.clear_malloc_data()
    println("Solving with $s")
    solution, log = @time if args["profile"]
        Profile.clear()
        Profile.init(n=1000000000)
        # reinitialize solver so we correctly track its performance
        solve = solver(s, ba, args["convergence"], args["tol"], opts)
        solution, log = @profile bundleadjust(ba, solve=solve, verbose=true, maxiter=args["maxiter"])
        Profile.print(noisefloor=2.0, mincount=1000)
        solution, log
    else
        solve = solver(s, ba, args["convergence"], args["tol"], opts)
        bundleadjust(ba, solve=solve, verbose=true, maxiter=args["maxiter"])
    end
    df = log_to_df(log)
    df[!, :solver] = fill(s, size(df,1))
    df[!, :convergence] .= args["convergence"]
    df[!, :ctol] .= args["tol"]
    df[!, :cameras] .= num_cameras(ba)
    df[!, :points] .= num_points(ba)
    if s == "multigrid"
        df = hcat(df, vcat(fill(csv(opts), size(df,1))...))
    end

    if args["nash_log"] != ""
        df_Q = mapreduce((x,y) -> vcat(x,y;cols=:union), enumerate(log)) do (i, x)
            if haskey(x, "solve_info") && haskey(x["solve_info"], "Q") && length(x["solve_info"]["Q"]) > 0
                Qs = x["solve_info"]["Q"]
                DataFrame(problem = i, Q = Qs, iter = 1:length(Qs))
            else
                DataFrame()
            end
        end
        CSV.write(args["nash_log"], df_Q)
    end

    if args["out"] != ""
        writebal(args["out"], solution)
    end

    df
end |> x -> vcat(x...; cols=:union)
if args["csv"] != "" && args["csv"] != nothing
    CSV.write(args["csv"], df)
end
